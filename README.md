You can get started by playing with the following commands

python create_search_space.py
./test
python create_experiment.py

Main executable programs are

create_search_space.py
create_experiment.py
experiment.py

The rest of the source files are stored under the following two paths
- msc_project/
- test/
