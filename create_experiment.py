# from console_loop import ConsoleLoop, quit_func
from msc_project.search_view import SearchView
from msc_project.search_space import loaded_space_choice, display_search_space
from torch import nn
import torch
from nni.compression.pytorch.utils.counter import count_flops_params

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
print('Using device:', device)

from nni.tools.nnictl.nnictl_utils import update_experiment, Experiments
import os
import json
from collections import namedtuple

def experiment_trial_results(experiment_id):
    "Return a list of trials with path to parameter file and final score"
    update_experiment()
    cfg = Experiments()
    experiments_dict = cfg.get_all_experiments()
    x = experiments_dict[experiment_id]
    trials_path = os.path.join(x['logDir'], x['id'], 'trials')
    res = []
    result = namedtuple('result', ['parameters', 'value'])
    for trial_dir in os.listdir(trials_path):
        fn = os.path.join(trials_path, trial_dir, '.nni', 'metrics')
        param_path = os.path.join(trials_path, trial_dir, 'parameter.cfg')
        with open(fn, 'r') as fp:
            last_line = fp.readlines()[-1]
            start = last_line.index('{')
            end = last_line.index('}') + 1
            result_json = last_line[start: end]
            value = json.loads(result_json)
            res.append(result(parameters=param_path, value=value))
    return res

def get_conv2d_layers(model):
    """Get list of conv2d layer names in pytorch model

    :param model: pytorch model
    :returns: Generator of name strings
    """
    return (name for (name, layer) in model.named_modules() if isinstance(layer, nn.Conv2d))

from nni.compression.pytorch.utils.shape_dependency import ChannelDependency

def get_dependency_sets(model, dummy_input):
    """Get sets of convolutional layers that are dependent

    :param model: pytorch model
    :param dummy_input: Dummy input to check dependencies
    :returns: List with each element being a set of layer names that fall into
    the same dependency group

    """
    channel_depen = ChannelDependency(model, dummy_input)
    return channel_depen.dependency_sets

def get_number_of_parameters(layer):
    """Get total number of parameters in a layer

    :param layer: Pytorch module
    :returns: Number of parameters

    """
    return sum(p.numel() for p in layer.parameters())

from argparse import ArgumentParser

import msc_project.keywords as kw

import os

DEFAULT_PRUNERS_DIR = os.path.join('search_spaces', 'pruners')
DEFAULT_OPTIMIZERS_DIR = os.path.join('search_spaces', 'optimizers')
DEFAULT_SCHEDULERS_DIR = os.path.join('search_spaces', 'schedulers')

def get_parser():
    searchspace_choices = lambda parentdir, subdir: os.listdir(os.path.join(parentdir, subdir))
    parser = ArgumentParser(
        "Creates an nni experiment for a given model and dataset"
    )
    parser.add_argument('--experiment-id', type=str, default=None)
    parser.add_argument('--dataset', type=str, default=kw.datasets.cifar,
                        help='dataset to use, mnist, cifar or imagenet')
    parser.add_argument('--cache-dir', type=str, default='./data/',
                        help='dataset directory')
    parser.add_argument('--experiment-dir', type=str, default='./experiment/',
                        help='experiment directory')
    parser.add_argument('--model', type=str, default=kw.models.resnet,
                        help='model to use')
    parser.add_argument('--pruners-dir', type=str, default=None,
                        help='root pruner directory')
    parser.add_argument('--pruners', type=str, nargs='+', default=None,
                        help='relative paths of pruners to use')
    parser.add_argument('--optimizers-dir', type=str, default=None,
                        help='root optimizer directory')
    parser.add_argument('--optimizers', type=str, nargs='+', default=None,
                        help='relative paths of optimizers to use')
    parser.add_argument('--schedulers-dir', type=str, default=None,
                        help='root scheduler directory')
    parser.add_argument('--schedulers', type=str, nargs='+', default=None,
                        help='relative paths of schedulers to use')
    parser.add_argument('--kd', action='store_true',
                        help='Use knowledge distillation')
    parser.add_argument('--search-layers', action='store_true',
                        help='Vary sparsity in each layer')
    parser.add_argument('--metrics',type=str, nargs='+',
                        help='metric to report', choices=kw.metrics.values(), default=kw.metrics.values())
    for target in kw.targets.values():
        parser.add_argument(f'--target-{target}', type=float,
                            help='set target value for {target}',
                            choices=kw.targets.values(), default=None)
    parser.add_argument('--train-batch-size', type=int, default=128,
                        help='Training batch size')
    parser.add_argument('--test-batch-size', type=int, default = 200,
                        help='Test batch size')
    parser.add_argument('--epochs', type=int, default = 10,
                        help='Number of epochs to finetune')
    parser.add_argument('--gpus', type=int, default=0,
                        help='Number of gpus available')
    return parser

import torch

from msc_project.pretrained_model import str2model
from msc_project import data
from msc_project.search_space import PathParser, SearchSpace, load_search_space

def load_model(args, device):
    print('Loading model')
    net = str2model(args.cache_dir, args.dataset, args.model, device)
    print('Loaded', net.__class__.__name__)

    dummy_input = data.dummy_input(args.dataset, 1, device)
    return net, dummy_input

def set_group_space(search_space: SearchView, net, dummy_input):
    """Creates a search space that samples sparsity values for individual groups of
    dependent conv2d layers
    """
    print('Getting dependencies')
    groups = get_dependency_sets(net, dummy_input)
    print('Finished')

    group_params = 0
    for i, group in enumerate(groups):
        group_space = search_space['config_list', str(i)]
        group_space.set_sample('sparsity', 'uniform', 0, 1)
        num = sum(get_number_of_parameters(net.get_submodule(name)) for name in group)
        group_params += num
        group_space['op_names'] = list(group)
        group_space['op_types'] = ['Conv2d']
        search_space['config_list_params', str(i)] = num
    search_space['config_list_total_params'] = group_params

def set_uniform_group_space(search_space, net, dummy_input):
    """Creates a search space that samples one overall sparsity value for conv2d
    layers"""

    group_space = search_space['config_list', str(0)]
    group_space['op_types'] = ['Conv2d']
    group_space['sparsity'] = 0.25

TOP_LEVEL_ARGS = (
    'dataset',
    'model',
    'cache_dir',
    'metrics',
    'kd',
    'search_layers',
    'test_batch_size',
    'train_batch_size',
    'epochs',
    'experiment_dir'
)

def load_best_search_space(experiment_id):
    """Looks through the logs of an experiment for the parameter that yielded the
    best result and returns it as a search space instance

    :param experiment_id: Id of the experiment to inspect
    :returns: Search space holding best trial parameters
    """
    results = experiment_trial_results(experiment_id)
    parameters_fn = max(results, key=lambda x: x.value).parameters
    ss = SearchSpace()
    ss.load(parameters_fn, "parameters")
    return ss

def main(args):
    """Creates a search_space.json file and a config.yml file for running
    experiment.py with a given sampling strategy"""
    if kw.metrics.accuracy not in args.metrics:
        args.metrics.append(kw.metrics.accuracy)

    search_space = SearchView(parser=PathParser('/'))

    if args.experiment_id is None:
        if args.pruners_dir is None:
            args.pruners_dir = DEFAULT_PRUNERS_DIR
        if args.optimizers_dir is None:
            args.optimizers_dir = DEFAULT_OPTIMIZERS_DIR
        if args.schedulers_dir is None:
            args.schedulers_dir = DEFAULT_SCHEDULERS_DIR
    else:
        search_space.update_key_value(
            '', load_best_search_space(args.experiment_id)
        )

    search_space.update_key_value('', {name: vars(args)[name]
                                       for name in TOP_LEVEL_ARGS
                                       if vars(args)[name] is not None})

    net, dummy_input = load_model(args, device)

    if args.search_layers:
        set_group_space(search_space, net, dummy_input)
    else:
        set_uniform_group_space(search_space, net, dummy_input)

    flops, params, _ = count_flops_params(net, dummy_input)
    search_space['flops'] = flops
    search_space['params'] = params

    print('Loading pruners')
    loaded_space_choice(search_space['pruner'], args.pruners_dir, args.pruners)
    print('Loading optimizers')
    loaded_space_choice(search_space['optimizer'], args.optimizers_dir, args.optimizers)
    print('Loading schedulers')
    loaded_space_choice(search_space['scheduler'], args.schedulers_dir, args.schedulers)

    for metric in kw.targets.values():
        search_space['target', metric] = vars(args)['target_' + metric]

    print('Saving search space')
    search_space.save('search_space.json')
    display_search_space(search_space)

    config_yml = \
f'''
searchSpaceFile: search_space.json
trialCommand: python3 experiment.py  # NOTE: change "python3" to "python" if you are using Windows
trialGpuNumber: {1 if args.gpus > 0 else 0}
trialConcurrency: {max(args.gpus, 1)}
maxExperimentDuration: 1h
tuner:
  name: TPE
  classArgs:
    optimize_mode: maximize
assessor:
  name: Curvefitting
  classArgs:
    epoch_num: {args.epochs}
    start_step: 6
    threshold: 0.95
    gap: 1
trainingService:
  platform: local
  maxTrialNumberPerGpu: 1
  useActiveGpu: false
'''.lstrip()
    with open('config.yml', 'w') as fp:
        fp.write(config_yml)

if __name__=='__main__':
    parser = get_parser()
    args = parser.parse_args()
    main(args)

