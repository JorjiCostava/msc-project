from msc_project.map_dict import MapDict

import unittest
import random
from functools import partial

class TestPrefix(unittest.TestCase):
    """Creates a dictionary containing a subset of items to exclude and a subset of
    items to include"""
    def add_prefix(self, x):
        return self.include_prefix + x
    def rem_prefix(self, x):
        if x.startswith(self.include_prefix):
            return x[self.include_prefix_len:]
        return None
    def setUp(self):
        self.include_prefix = "include_"
        self.exclude_prefix = "exclude_"
        self.include_prefix_len = len(self.include_prefix)
        self.include_values = random.sample(range(100), 10)
        self.exclude_values = random.sample(range(100), 10)
        self.inner_dict = {
            **{f"{self.include_prefix}{i}": v for i, v in enumerate(self.include_values)},
            **{f"{self.exclude_prefix}{i}": v  for i, v in enumerate(self.exclude_values)}
        }
        self.map_dict = MapDict(self.inner_dict, self.add_prefix, self.rem_prefix)
    def test_values(self):
        "Test values are identical to include set"
        self.assertSequenceEqual(self.include_values, list(self.map_dict.values()))
    def test_keys(self):
        "Test keys are identical to include set"
        self.assertSequenceEqual(range(10), list(map(int,self.map_dict.keys())))
    def test_setitem(self):
        "Perform a function on include set and check if inner dict is changed correctly"
        a, b = random.randint(0, 100), random.randint(0, 100)
        for k in self.map_dict:
            self.map_dict[k] = a * self.map_dict[k] + b
        self.assertEqual(a*sum(self.include_values) + b*len(self.include_values) + sum(self.exclude_values), sum(self.inner_dict.values()))
    def test_filtered(self):
        "Ensure that all keys in the inner dict no longer work"
        self.assertDictContainsSubset({self.add_prefix(k):v for k, v in self.map_dict.items()}, self.inner_dict)
        for i in range(len(self.include_values)):
            with self.assertRaises(KeyError):
                x = self.map_dict[f'{self.include_prefix}{i}']

import sys, zlib
import os

class TestCompress(unittest.TestCase):
    def setUp(self):
        compress = lambda s: zlib.compress(s.encode())
        decompress = lambda s: zlib.decompress(s).decode() if isinstance(s, bytes) else None
        self.mapdict = MapDict(map_k=compress, unmap_k=decompress, map_v=compress, unmap_v=decompress)
        self.compress, self.decompress = compress, decompress
        keys = [os.path.join('tests', 'res', f'text{i}.txt') for i in range(5)]
        vals = []
        for fn in keys:
            with open(fn,'r') as fp:
                val = fp.read().strip()
                vals.append(val)
        for k, v in zip(keys, vals):
            self.mapdict[k] = v
        self.keys, self.vals = keys, vals
        self.compressed_keys, self.compressed_vals = list(map(self.compress, keys)), list(map(self.compress, vals))
    def test_size(self):
        """Check if compression functions on map_dict results in same size in
        underlying dict compressed elements
        """
        self.assertEqual(sum(map(sys.getsizeof, self.compressed_keys)), sum(map(sys.getsizeof, self.mapdict._dict.keys())))
        self.assertEqual(sum(map(sys.getsizeof, self.compressed_vals)), sum(map(sys.getsizeof, self.mapdict._dict.values())))
        self.assertLess(sum(map(sys.getsizeof, self.mapdict._dict.keys())), sum(map(sys.getsizeof, self.mapdict.keys())))
        self.assertLess(sum(map(sys.getsizeof, self.mapdict._dict.values())), sum(map(sys.getsizeof, self.mapdict.values())))
    def test_interface(self):
        """Check if map_dict results in the same interface as was used to construct the
        compressed dictionary
        """
        self.assertSequenceEqual(list(self.mapdict.items()),
        list(zip(self.keys, self.vals)))
