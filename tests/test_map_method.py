from msc_project.map_method import MappedFunc
import unittest

class Actor:
    def __init__(self, name, feelings=None):
        self.name = name
        if feelings is None:
            feelings = {}
        self.feelings = feelings
    def hates(self, other):
        self.feelings[other.name] = 'enemy'
    def loves(self, other):
        self.feelings[other.name] = 'friend'
    def feeling(self, other):
        if other.name not in self.feelings:
            return 'Unknown'
        return self.feelings[other.name]

class TestGreet(unittest.TestCase):
    def setUp(self):
        super().setUp()
        farmer = Actor('the farmer')
        chicken = Actor('the chicken')
        fox = Actor('the fox')
        farmer.loves(chicken)
        farmer.hates(fox)
        fox.hates(farmer)
        fox.loves(chicken)
        chicken.loves(farmer)
        chicken.hates(fox)
        self.farmer, self.chicken, self.fox = farmer, chicken, fox
        self.expected_strs = [(farmer, chicken, 'the farmer says hello to the farmer\'s friend the chicken'),
                         (farmer, fox, 'the farmer says hello to the farmer\'s enemy the fox'),
                         (chicken, farmer, 'the chicken says hello to the chicken\'s friend the farmer'),
                         (chicken, fox, 'the chicken says hello to the chicken\'s enemy the fox'),
                         (fox, farmer, 'the fox says hello to the fox\'s enemy the farmer'),
                         (fox, chicken, 'the fox says hello to the fox\'s friend the chicken'),
                         ]
    def test_method(self):
        "Method wrapper that maps argument with a function"
        class GreetActor(Actor):
            def __init__(self, actor):
                super(GreetActor, self).__init__(actor.name, actor.feelings)
            def other_feeling(self, o):
                return Actor(f"{self.name}'s {self.feeling(o)} {o.name}")
            @MappedFunc(arg=other_feeling)
            def greet(self, o):
                return f'{self.name} says hello to {o.name}'
        expected_strs = [(GreetActor(a), GreetActor(b), s) for a,b,s in self.expected_strs]
        for a, b, s in expected_strs:
            self.assertEqual(a.greet(b), s)
    def test_function(self):
        "Function wrapper that maps argument with a function"
        def other_feeling(pair):
            pair = list(pair)
            pair[1] = Actor(f"{pair[0].name}'s {pair[0].feeling(pair[1])} {pair[1].name}")
            return pair
        @MappedFunc(arg=other_feeling)
        def _greet(pair):
            return f'{pair[0].name} says hello to {pair[1].name}'
        def greet(a, b):
            return _greet((a,b))

        for a, b, s in self.expected_strs:
            self.assertEqual(greet(a, b), s)



