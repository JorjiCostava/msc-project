from msc_project.path_parser import PathParser
import os
from io import StringIO
from tests.test_search_space import SearchSpaceTestCase
import unittest
from unittest.mock import patch
from msc_project.search_space import SearchSpace, display_search_space, displaystr_search_space
from create_search_space import CreateSearchSpace

from inspect import cleandoc

class CreateSearchSpaceTestCase(SearchSpaceTestCase):
    """Unittest for CreateSearchSpace instances
    """
    def feedInput(self, search_space: SearchSpace, cmds: str) -> str:
        """Feeds it a multi-line string of commands with indentation removed to the
        search space editor program

        :param search_space: A search space instance to manipulate
        CreateSearchSpace program
        :param cmds: A multi-line string of commands that will be fed into stdin
        :returns: stdout while the program is running, with prompts supressed

        """
        cmds = cleandoc(cmds) + '\nexit\n'
        console = CreateSearchSpace(search_space)
        console.silence()
        with patch('sys.stdin', StringIO(cmds)) as stdin, patch('sys.stdout', StringIO()) as stdout:
            console()
            out = stdout.getvalue()
        return out

    import re
    text_p = re.compile(r'[\w\.]*$')
    cmd_p = re.compile(r'^\w+')
    def getCompleteHints(self, search_space: SearchSpace, line: str):
        """Return a list of autocompletion hints for passing an incompleted cmd string
        to the search space editor program

        :param search_space: A search space instance to manipulate
        :param line: A single command string that will be used to trigger autocomplete

        E.g.
        >>> self.getCompleteHints(
        ...    SearchSpace({'pos':{'x':1,'y':1}}, parser=PathParser('/')),
        ...    'print pos/'
        ...)
        ['pos/x', 'pos/y']

        :returns: List of autocompletion hints

        """
        console = CreateSearchSpace(search_space)
        console.silence()
        text_p, cmd_p = self.text_p, self.cmd_p
        m = text_p.search(line)
        if m is None:
            raise Exception('Impossible')
        text = m.group()
        begidx, endidx = m.span()
        kwds = {'text': text, 'line': line, 'begidx': begidx, 'endidx': endidx}
        cmd = cmd_p.search(line).group()
        cmpl_func = getattr(console, 'complete_' + cmd)
        return cmpl_func(**kwds)

def get_cmd_name(func):
    """Convert cmd.Cmd do_ method to its command string

    Used so command strings are in this unittest refactored along with the method

    :param func: Unbound method of some cmd.Cmd subclass
    :returns: Command string

    """
    return func.__name__[3:]

class TestNestedMnist(CreateSearchSpaceTestCase):
    def setUp(self):
        ex_path = os.path.join('tests', 'res', 'mnist-nested-search-space.json')
        with open(ex_path) as fp:
            self.ex_str = fp.read()
        ex_ss = SearchSpace(parser=PathParser('/'))
        ex_ss.load(ex_path)
        self.ex_ss = ex_ss
    def test_absolute_path(self):
        "Constructs mnist example using absolute path commands"
        x = SearchSpace(parser=PathParser('/'))
        for i in range(4):
            prefix = f'layer{i}'
            search_cmd = get_cmd_name(CreateSearchSpace.do_search)
            self.feedInput(x,
                f'''
                {search_cmd} {prefix} subsearch Empty Conv Max_pool Avg_pool
                {search_cmd} {prefix}/Conv/kernel_size choice 1 2 3 5
                {search_cmd} {prefix}/Max_pool/pooling_size choice 2 3 5
                {search_cmd} {prefix}/Avg_pool/pooling_size choice 2 3 5
                ''')
        self.assertSearchSpaceValid(x)
        self.assertSearchSpaceEqual(x, self.ex_ss)
    def test_put(self):
        "Construct a search parameter manually from commands in editor"
        x = SearchSpace(parser=PathParser('/'))
        import json
        set_cmd = get_cmd_name(CreateSearchSpace.do_set)
        for i in range(4):
            prefix = f'layer{i}'
            value_json = json.dumps(self.ex_ss[prefix, '_value'])
            type_json = json.dumps(self.ex_ss[prefix, '_type'])
            self.feedInput(x,
                f'''
                {set_cmd} {prefix}/_type {type_json}
                {set_cmd} {prefix}/_value {value_json}
                ''')
        x.generate_map()
        self.assertSearchSpaceValid(x)
        self.assertSearchSpaceEqual(x, self.ex_ss)
    def test_tree(self):
        "Compare output tree of the search space editor program to API output"
        out1 = self.feedInput(self.ex_ss,
                              '''
                              tree /
                              ''')
        out2 = displaystr_search_space(self.ex_ss)
        self.assertEqual(out1, out2)
    def test_path_complete(self):
        "Check if path completion is working for all commands"
        tree_cmd = get_cmd_name(CreateSearchSpace.do_tree)
        print_cmd = get_cmd_name(CreateSearchSpace.do_print)
        set_cmd = get_cmd_name(CreateSearchSpace.do_set)
        remove_cmd = get_cmd_name(CreateSearchSpace.do_remove)
        load_cmd = get_cmd_name(CreateSearchSpace.do_load)
        move_cmd = get_cmd_name(CreateSearchSpace.do_move)
        search_cmd = get_cmd_name(CreateSearchSpace.do_search)
        path_cmds = [
            tree_cmd + ' {root_str}',
            print_cmd + ' {root_str}',
            set_cmd + ' {root_str}',
            remove_cmd + ' {root_str}',
            load_cmd + ' fn {root_str}',
            load_cmd + ' fn from {root_str}',
            move_cmd + ' {root_str}',
            move_cmd + ' from {root_str}',
            search_cmd + ' {root_str}',
        ]
        for line in path_cmds:
            for path, val in self.ex_ss.path_map.items():
                path = self.ex_ss.add_aliases(path)
                root, key = self.ex_ss.parser.part_leaf(path)
                if root != self.ex_ss.parser.ROOT_PATH:
                    if isinstance(key, int):
                        root_str = self.ex_ss.parser.stringify(root) + '['
                    else:
                        root_str = self.ex_ss.parser.stringify(self.ex_ss.parser.join(root,''))
                else:
                    root_str = ''
                a = self.getCompleteHints(self.ex_ss, line.format(root_str=root_str))
                self.assertIn(self.ex_ss.parser.stringify(path), a, msg=repr(line.format(root_str=root_str)))
    def test_file_complete(self):
        """Check if file completion is working for all commands that accept such a
        parameter"""
        parent_dir = os.path.join('search_spaces', 'pruners')
        line = 'fsubsearch ' + parent_dir + ' fgpm.json '
        a = self.getCompleteHints(self.ex_ss, line)
        self.assertSequenceEqual(a, [os.path.join(parent_dir, x) for x in os.listdir(parent_dir)])
        parent_dir = os.path.join('search_spaces', 'pruners')
        line = 'fsubsearch ' + os.path.join(parent_dir, '')
        a = self.getCompleteHints(self.ex_ss, line)
        self.assertSequenceEqual(a, [os.path.join(parent_dir, x) for x in os.listdir(parent_dir)])
