from tests.test_search_view import SearchViewTestCase
from msc_project.search_view import SearchView
from msc_project.search_space import PathParser, SearchSpace
from create_experiment import DEFAULT_OPTIMIZERS_DIR, DEFAULT_PRUNERS_DIR, DEFAULT_SCHEDULERS_DIR, loaded_space_choice, experiment_trial_results
import unittest
import os
from unittest.mock import patch
from io import StringIO


class TestCreateExperiment(SearchViewTestCase):
    def setUp(self):
        self.ss = SearchView(SearchSpace(parser=PathParser('/')))
        return super().setUp()
    def assertLoadedSpaceChoice(self, subspace: SearchView, parent_dir, fns):
        self.assertIsInstance(subspace, SearchView)
        with patch('sys.stdout', StringIO()) as stdout:
            loaded_space_choice(subspace, parent_dir, fns)
        self.assertSearchSpaceValid(subspace)
        self.assertSearchSpaceValid(self.ss)
        for fn in fns:
            self.assertIn(fn, subspace)
            ss_pruner = SearchSpace(parser=PathParser('/'))
            ss_pruner.load(os.path.join(parent_dir, fn))
            ss_pruner['_name'] = fn
            self.assertSearchSpaceEqual(subspace[fn],
                                        ss_pruner)
    def test_load_pruners(self):
        "Load pruners"
        pruners = os.listdir(DEFAULT_PRUNERS_DIR)
        subspace = self.ss['pruner']
        self.assertLoadedSpaceChoice(subspace, DEFAULT_PRUNERS_DIR, pruners)
    def test_load_optimizers(self):
        "Load optimizers"
        optimizers = os.listdir(DEFAULT_OPTIMIZERS_DIR)
        subspace = self.ss['optimizer']
        self.assertLoadedSpaceChoice(subspace, DEFAULT_OPTIMIZERS_DIR, optimizers)
    def test_load_schedulers(self):
        "Load schedulers"
        schedulers = os.listdir(DEFAULT_SCHEDULERS_DIR)
        subspace = self.ss['scheduler']
        self.assertLoadedSpaceChoice(subspace, DEFAULT_SCHEDULERS_DIR, schedulers)

