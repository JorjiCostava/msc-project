from copy import deepcopy
from msc_project.path_parser import PathType
from msc_project.search_space import PathParser, is_nested_search_param, display_search_space, displaystr_search_space
from msc_project.search_space import SearchSpace

import unittest
import os

class SearchSpaceTestCase(unittest.TestCase):
    def assertSearchSpaceValid(self, x: SearchSpace):
        "Ensure pathmap and aliases are correct"
        for keys, value in x.path_map.items():
            if keys != x.parser.ROOT_PATH:
                # print(f'Checking {keys}')
                # print(value)
                if x.is_nested_search_param(keys):
                    alias = x.parser.join(x.parser.subpath(keys, 0, -2), value['_name'])
                    # print('ALIAS', alias, keys)
                    self.assertIn(alias, list(x.aliases.keys()))
                    self.assertIn((alias, keys), list(x.aliases.items()))
            path = x.parser.stringify(keys)
            v = x.root_val
            subpath_keys = []
            for i, key in enumerate(keys):
                if key == '':
                    continue
                subpath_keys.append(key)
                subpath = x.parser.stringify(x.parser.join(*subpath_keys))
                checked = x.parser.stringify(x.parser.join(*subpath_keys[:-1]))
                msg = ['']
                msg.append(f'{path} in `path_dict` but only {checked} part found in `value`')
                msg.append(f'pathmap = {value}')
                msg.append(f'value = {v}')
                if '_type' in v and '_value' in v:
                    nested_dict = next(
                        (o for o in v['_value']
                         if isinstance(o, dict) and o['_name'] == key),
                        None)
                    msg.append(f'Could not find dict with item ("_name", {repr(key)}) in {subpath}._value = {v["_value"]}')
                    if nested_dict is not None:
                        v = nested_dict
                        continue
                if isinstance(v, dict):
                    self.assertIn(key, v,
                                  '\n'.join(msg))
                elif isinstance(v, list):
                    self.assertIn(key, range(len(v)))
                v = v[key]
            self.assertEqual(v, value,
                             f'\nCompared {path} value found navigating SearchSpace instance\'s `value` and `path_map` attributes')

    def assertSearchSpaceEqual(self, ssa, ssb, msg = None):
        "Ensure search space attrs are equal"
        self.assertDictEqual(ssa.root_val, ssb.root_val, msg)
        self.assertDictEqual(ssa.path_map, ssb.path_map, msg)
        self.assertDictEqual(ssa.aliases, ssb.aliases)

    def ensure_copied(self, path: PathType, x: SearchSpace, y: SearchSpace):
        "Ensure search space is copied by walking and checking values and refs"
        if isinstance(x, str):
            return
        path = path.strip('.')
        self.assertEqual(x, y)
        if isinstance(x, (dict, list)):
            self.assertNotEqual(id(x), id(y))
        if path != '':
            if isinstance(list):
                path += '[{key}]'
            else:
                path += '.{key}'
        if isinstance(x, dict):
            for k in x:
                self.ensure_copied(path.format(key=k), x[k], y[k])
        if isinstance(x, list):
            for i in range(len(x)):
                self.ensure_copied(path.format(key=i), x[i], y[i])

class TestNestedMnist(SearchSpaceTestCase):
    def setUp(self):
        ex_path = os.path.join('tests', 'res', 'mnist-nested-search-space.json')
        with open(ex_path) as fp:
            self.ex_str = fp.read()
        ex_ss = SearchSpace(parser=PathParser('.'))
        ex_ss.load(ex_path)
        self.ex_ss = ex_ss
    def test_valid(self):
        "Checks search space has valid state"
        self.assertSearchSpaceValid(self.ex_ss)
    def test_pop(self):
        "Removes path"
        for i in range(4):
            prefix = f'layer{i}'
            root_path = self.ex_ss.parser.join(prefix, 'Avg_pool')
            popped_paths = self.ex_ss.path_map.keys()
            popped_paths = [x for x in popped_paths if self.ex_ss.parser.ndescendent(root_path, x)]
            self.ex_ss.pop(prefix + '.Avg_pool')
            for p in popped_paths:
                with self.assertRaises(KeyError):
                    _ = self.ex_ss[p]
        self.assertSearchSpaceValid(self.ex_ss)
    def test_absolute_path(self):
        "Constructing mnist by setting absolute paths"
        x = SearchSpace()
        for i in range(4):
            prefix = f'layer{i}'
            x.set_subsearch_sample(prefix, 'Empty', 'Conv', 'Max_pool', 'Avg_pool')
            tmp = str(x[prefix])
            x.set_sample(prefix + '.Conv.kernel_size', 'choice', 1, 2, 3, 5)
            self.assertEqual(x[prefix + '.Conv'], self.ex_ss[prefix + '.Conv'],
                             f'\n{self.ex_ss[prefix + ".Conv"]}\n{str(x[prefix + ".Conv"])}')
            x.set_sample(prefix + '.Max_pool.pooling_size', 'choice', 2, 3, 5)
            x.set_sample(prefix + '.Avg_pool.pooling_size', 'choice', 2, 3, 5)
        self.assertSearchSpaceValid(x)
        self.assertSearchSpaceEqual(x, self.ex_ss)
    def test_list_append(self):
        "Append _value elements via list syntax e.g. layer0.Conv.kernel_size._value[0]"
        x = SearchSpace()
        for i in range(4):
            prefix = f'layer{i}'
            x.set_subsearch_sample(prefix, 'Empty', 'Conv', 'Max_pool', 'Avg_pool')
            tmp = str(x[prefix])
            x[prefix + '.Conv.kernel_size._type'] = 'choice'
            x[prefix + '.Max_pool.pooling_size._type'] = 'choice'
            x[prefix + '.Avg_pool.pooling_size._type'] = 'choice'
            for j, v in enumerate(range(4)):
                x[f'{prefix}.Conv.kernel_size._value[{j}]'] = v+1
            x[f'{prefix}.Conv.kernel_size._value[3]'] = 5
            self.assertEqual(x[prefix + '.Conv'], self.ex_ss[prefix + '.Conv'],
                             f'\n{self.ex_ss[prefix + ".Conv"]}\n{str(x[prefix + ".Conv"])}')
            for j, v in enumerate((2,3,5)):
                x[f'{prefix}.Max_pool.pooling_size._value[{j}]'] = v
                x[f'{prefix}.Avg_pool.pooling_size._value[{j}]'] = v
        self.assertSearchSpaceValid(x)
        self.assertSearchSpaceEqual(x, self.ex_ss)
    def test_subspace_path(self):
        "Construct subspaces seperately then attach them"
        x = SearchSpace()
        conv, max_pool, avg_pool = (SearchSpace() for _ in range(3))
        conv.set_sample('kernel_size', 'choice', 1, 2, 3, 5)
        max_pool.set_sample('pooling_size', 'choice', 2, 3, 5)
        avg_pool.set_sample('pooling_size', 'choice', 2, 3, 5)
        msg = ['']
        for i in range(4):
            prefix = f'layer{i}'
            x.set_subsearch_sample(prefix, 'Empty', 'Conv', 'Max_pool', 'Avg_pool')
            for k, v in zip(('.Conv', '.Max_pool', '.Avg_pool'), (conv, max_pool, avg_pool)):
                x.update_key_value(prefix + k, v)
                self.assertNotEqual(id(x[prefix + k]), id(v))
        self.assertSearchSpaceValid(x)
        self.assertSearchSpaceEqual(x, self.ex_ss)
    def test_deepcopy(self):
        ex_copy = deepcopy(self.ex_ss)
        for k, v1 in self.ex_ss.path_map.items():
            if isinstance(v1, (dict, list)):
                v2 = ex_copy.path_map[k]
                self.assertNotEqual(id(v1), id(v2))
        self.assertSearchSpaceValid(ex_copy)
        self.ensure_copied('', self.ex_ss, ex_copy)
    def test_tuple(self):
        "Test that tuple paths and str paths both work"
        x = SearchSpace()
        for p in x.path_map:
            self.assertEqual(x[p], x['.'.join(p)])

class TestParameterCfg(SearchSpaceTestCase):
    def setUp(self):
        ex_path = os.path.join('tests', 'res', 'parameter.cfg')
        with open(ex_path) as fp:
            self.ex_str = fp.read()
        ex_ss = SearchSpace()
        ex_ss.load(ex_path)
        self.ex_ss = ex_ss
    def test_move_branch(self):
        "Compare the result of __getitem__('parameters') to relocating the 'parameter' path to the root"
        parameters_space = SearchSpace(deepcopy(self.ex_ss['parameters']))
        SearchSpace.move_branch(self.ex_ss, 'parameters', self.ex_ss, '')
        self.assertSearchSpaceEqual(parameters_space, self.ex_ss)

class TestNastyKeys(SearchSpaceTestCase):
    def setUp(self):
        self.ex_ss = SearchSpace({
            'root': {'p1': 1, 'p2': 2},
            'root.p1': 3,
            'root.p2': 4

        })

    def test_absolute_path(self):
        "Tries to use keys that look identical with default path seperator"
        x = SearchSpace(parser=PathParser('/'))
        x['root','p1'] = 1
        x['root','p2'] = 2
        x['root.p1'] = 3
        x['root.p2'] = 4
        self.assertSearchSpaceValid(x)
        self.assertSearchSpaceEqual(x, self.ex_ss)
