from copy import deepcopy
from msc_project.map_dict import MapDict
from msc_project.search_space import SearchSpace, display_search_space
from msc_project.search_view import SearchView

import unittest

from tests import test_search_space as supertests

class SearchViewTestCase(supertests.SearchSpaceTestCase):
    def assertSearchSpaceValid(self, x):
        if isinstance(x, SearchView):
            ss = x.search_space
            self.assertEqual(ss[x.path_prefix], x.root_val)
            self.assertDictEqual(x.path_map._dict, ss.path_map)
        super(SearchViewTestCase, self).assertSearchSpaceValid(x)

    def assertMapDictEqual(self, x, y):
        self.assertSequenceEqual(sorted(tuple(x.keys())), sorted(tuple(y.keys())))
        self.assertSequenceEqual(sorted(tuple(map(repr, x.values()))), sorted(tuple(map(repr, y.values()))))
        self.assertSequenceEqual(sorted(tuple(x.items())), sorted(tuple(y.items())))
    def assertSearchSpaceEqual(self, ssa, ssb):
        self.assertDictEqual(ssa.root_val, ssb.root_val)
        pma, pmb = ssa.path_map, ssb.path_map
        self.assertMapDictEqual(pma, pmb)

class TestMisc(SearchViewTestCase):
    "Miscellanneous tests"
    def test_move(self):
        alphabet = 'abcdefghijklmnopqrstuvwxyz'
        ss = SearchSpace({"test": {k:v for k, v in zip(alphabet, range(len(alphabet)))}})
        sv = SearchView(ss, 'test')
        ss_copy = deepcopy(ss)
        SearchSpace.move_branch(ss_copy, 'test', ss_copy, '')
        self.assertEqual(str(sv), str(ss_copy))
    def test_coords(self):
        ss = SearchSpace()
        for i in range(10):
            ss['coords', i, 'x'] = i
            ss['coords', i, 'y'] = i ** 2
        for i, coord in enumerate(SearchView(ss, 'coords')):
            self.assertSequenceEqual((i, i**2), (coord['x'], coord['y']))
    def test_coords2(self):
        ss = SearchSpace()
        for i in range(10):
            ss[f'coords[{i}]x'] = i
            ss[f'coords[{i}]y'] = i ** 2
        for i, coord in enumerate(SearchView(ss, 'coords')):
            self.assertSequenceEqual((i, i**2), (coord['x'], coord['y']))
    #     display_search_space(ss)
    # def test_misc(self):
    #     ss = SearchView()
    #     ss.load('search_space.json')
    #     display_search_space(ss)

class TestNestedMnist(SearchViewTestCase, supertests.TestNestedMnist):
    def setUp(self):
        super().setUp()
        self.ex_ss = SearchView(self.ex_ss, '')
    def test_subview(self):
        "Construct mnist example by modifying views of branches"
        _x = SearchSpace()
        x = SearchView(_x)
        conv, max_pool, avg_pool = (SearchSpace() for _ in range(3))
        conv.set_sample('kernel_size', 'choice', 1, 2, 3, 5)
        max_pool.set_sample('pooling_size', 'choice', 2, 3, 5)
        avg_pool.set_sample('pooling_size', 'choice', 2, 3, 5)
        msg = ['']
        for i in range(4):
            subview = x[f'layer{i}']
            subview.set_subsearch_sample('', 'Empty', 'Conv', 'Max_pool', 'Avg_pool')
            for k, v in zip(('Conv', 'Max_pool', 'Avg_pool'), (conv, max_pool, avg_pool)):
                subview.update_key_value(k, v)
                self.assertNotEqual(id(subview[k]), id(v))
        self.assertSearchSpaceValid(x)
        self.assertSearchSpaceEqual(x, self.ex_ss)

class TestParameterCfg(SearchViewTestCase, supertests.TestParameterCfg):
    def setUp(self):
        super().setUp()
        self.ex_ss = SearchView(self.ex_ss, '')
    def test_move_branch(self):
        # Override so parameter_space is of SearchView type
        parameters_space = deepcopy(self.ex_ss['parameters'])
        SearchSpace.move_branch(self.ex_ss, '', self.ex_ss, 'tmp')
        SearchSpace.move_branch(self.ex_ss, 'tmp.parameters', self.ex_ss, '')
        self.assertSearchSpaceEqual(parameters_space, self.ex_ss)

class TestNastyKeys(SearchViewTestCase, supertests.TestNastyKeys):
    def setUp(self):
        super().setUp()
        self.ex_ss = SearchView(self.ex_ss, '')
