import json
from .util import escaped_split

def choice(args):
    "Prompt for choice type"
    if len(args) == 0:
        values = escaped_split(input('Enter values> '))
    else:
        values = args
    return map(json.loads, values)

def subsearch(args):
    "Prompt for nested subsearch type"
    if len(args) == 0:
        values = escaped_split(input('Enter element names> '))
    else:
        values = args
    return map(str, values)

def lower_upper(args):
    "Prompt for float interval"
    if len(args) == 0:
        lower = input('Enter lower bound> ')
    else:
        lower = args[0]
    if len(args) < 2:
        upper = input('Enter upper bound> ')
    else:
        upper = args[1]
    return (float(lower), float(upper))

def randint(args):
    "Prompt for int interval"
    if len(args) == 0:
        lower = input('Enter lower value inclusive> ')
    else:
        lower = args[0]
    if len(args) < 2:
        upper = input('Enter upper value exclusive> ')
    else:
        upper = args[1]
    return (int(lower), int(upper))

def q_value(args):
    "Prompt for q-value"
    if len(args) == 0:
        q = input('Enter q value of which sampled value will be a multiple> ')
    else:
        q = args[0]
    return float(q)

def lower_upper_q(args):
    "Prompt for float interval and q-value"
    lower, upper = lower_upper(args[:2])
    q = q_value(args[2:])
    return lower, upper, q

def mu_sigma(args):
    "Prompt for mu and sigma"
    if len(args) == 0:
        mu = input('Enter mean> ')
    else:
        mu = args[0]
    if len(args) < 2:
        sigma = input('Enter standard deviation> ')
    else:
        sigma = args[1]
    return (float(mu), float(sigma))

def mu_sigma_q(args):
    "Prompt for mu and sigma and q-value"
    mu, sigma = mu_sigma(args[:2])
    q = q_value(args[2:])
    return mu, sigma, q
