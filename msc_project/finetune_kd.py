from torch import nn
import torch.nn.functional as F

class KnowledgeDistiller:
    "Used for distilling the knowledge in a neural network"

    def __init__(self, teacher_model, temp):
        self.teacher_model = teacher_model
        self.temp = temp

    def kd_loss(self, y_s, y_t):
        "Compute knowledge distiller loss"
        p_s = F.log_softmax(y_s/self.temp, dim=1)
        p_t = F.softmax(y_t/self.temp, dim=1)
        loss = F.kl_div(p_s, p_t, size_average=False) * (self.temp**2) / y_s.shape[0]
        return loss

    def loss(self, student_model, data, target, criterion):
        "Compute criterion loss + kd loss"
        student_output = student_model(data)
        teacher_output = self.teacher_model(data)
        criterion_loss = criterion(student_output, target)
        kd_loss = self.kd_loss(student_output, teacher_output)
        return criterion_loss + kd_loss
