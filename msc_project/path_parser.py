from typing import Any, Dict, List, Tuple, Union
from functools import reduce

KeyType = Union[str, int]
PathType = Tuple[KeyType, ...]

import re

p_index = re.compile(r'\[(\d+)\]')

import copy

class PathParser:

    ROOT_PATH: PathType = tuple()

    def __init__(self, sep):
        self.sep = sep

    def __copy__(self):
        return PathParser(self.sep)

    def __deepcopy__(self, memo=None):
        return PathParser(copy.deepcopy(self.sep))

    def parse_str(self, p: str) -> PathType:
        if p == '' or p == self.sep:
            return self.ROOT_PATH
        toks = p.split(self.sep)
        ret = []
        for tok in toks:
            start = 0
            matches = tuple(p_index.finditer(tok))
            for m in matches:
                span = m.span()
                prev = tok[start: span[0]]
                if prev != '':
                    ret.append(prev)
                ret.append(int(m.group(1)))
                start = span[1]
            end = tok[start:]
            if end != '':
                ret.append(end)
        return tuple(ret)


    def parse(self, p: Union[str, PathType]) -> PathType:
        if isinstance(p, str):
            return self.parse_str(p)
        elif isinstance(p, int):
            return (p,)
        else:
            return p

    def __getitem__(self, p: str) -> PathType:
        return self.parse(p)

    def stringify(self, p: PathType):
        ret = []
        for k in p:
            if isinstance(k, int):
                ret.append(f'[{k}]')
            else:
                ret.append(f'{self.sep}{k}')
        return ''.join(ret).lstrip(self.sep)

    def ndescendent(self, n: PathType, c: PathType):
        return n == c[:len(n)]

    def descendent(self, n: PathType, c: PathType):
        return self.ndescendent(n, c) and len(n) != len(c)

    def nancester(self, n: PathType, p: PathType):
        return self.ndescendent(p, n)

    def ancester(self, n: PathType, p: PathType):
        return self.nancester(n, p) and len(n) != len(p)

    def part(self, n: PathType, i: int) -> Tuple[PathType, KeyType]:
        return n[:i], n[i]

    def part_leaf(self, n: PathType) -> Tuple[PathType, KeyType]:
        return self.part(n, -1)

    def key(self, n: PathType, index: int):
        return n[index]

    def subpath(self, n: PathType, start: int, end:int):
        return n[start:end]

    def depth(self, n: PathType) -> int:
        return len(n)

    def join(self, *paths: Union[str, PathType]) -> PathType:
        if len(paths) == 0:
            return self.ROOT_PATH
        parsed_paths = (path if isinstance(path, tuple) else (path,) for  path in paths)
        return reduce(tuple.__add__, parsed_paths, self.ROOT_PATH)

    def infer_type(self, path, key_idx) -> Union[List[Any], Dict[Any, Any]]:
        if key_idx + 1 < len(path) and isinstance(path[key_idx + 1], int):
            return []
            return [None] * (path[key_idx + 1] + 1)
        return {}
