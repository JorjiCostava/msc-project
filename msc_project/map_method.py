
from functools import update_wrapper, wraps


class BoundMappedMethod:
    def __init__(self, obj, method, res_map, pos_maps, kwd_maps):
        self.obj = obj
        self.method = method
        self.res_map = res_map
        self.pos_maps = pos_maps
        self.kwd_maps = kwd_maps
    def __call__(self, *args, **kwds):
        args = list(args)
        for pos, m in self.pos_maps:
            if pos < len(args):
                args[pos] = m(self.obj, args[pos])
        for kwd, m in self.kwd_maps:
            if kwd in kwds:
                kwds[kwd] = m(self.obj, kwds[kwd])
        res = self.method(self.obj, *args, **kwds)
        if self.res_map is not None:
            res = self.res_map(self.obj, res)
        return res

class MappedBuilder:
    def __init__(self):
        self.method = None
        self.res_map = None
        self.pos_maps = []
        self.kwd_maps = []
    def add_pos_map(self, position, method):
        return self.pos_maps.append((position, method))
    def add_kwd_map(self, keyword, method):
        return self.kwd_maps.append((keyword, method))
    def arg(self, *methods, start=0):
        for i, method in enumerate(methods):
            if method is not None:
                self.add_pos_map(start + i, method)
        return self
    def kwd(self, arg=None, **kwds):
        if arg is not None:
            for kwd, method in iter(arg):
                self.add_kwd_map(kwd, method)
        for kwd, method in kwds:
            self.add_kwd_map(kwd, method)
        return self
    def result(self, result):
        self.res_map = result
        return self
    def map_args_kwds(self, args, kwds):
        args = list(args)
        for pos, m in self.pos_maps:
            if pos < len(args):
                args[pos] = m(args[pos])
        for kwd, m in self.kwd_maps:
            if kwd in kwds:
                kwds[kwd] = m(kwds[kwd])
        return args, kwds

class MappedFunc(MappedBuilder):
    """Invokes another function with mapped parameters and result
    Mappings are added using a builder pattern

    :param method: Instance method to wrap
    :returns: Callable function or bindable method descriptor
    """
    def __init__(self, method=None, res=None, arg=None, arg_start=0, kwd=None):

        super(MappedFunc, self).__init__()
        self.method = method
        self.res = res
        if arg is not None:
            try:
                self.arg(*arg, start=arg_start)
            except TypeError:
                self.arg(arg, start=arg_start)

        if kwd is not None:
            try:
                self.kwd(arg = kwd)
            except TypeError:
                self.kwd(arg = kwd)
        self.check_method()

    def __get__(self, obj, cls):
        return update_wrapper(BoundMappedMethod(obj, self.method, self.res_map, self.pos_maps, self.kwd_maps), self.method)
    def check_method(self):
        if self.method is not None:
            update_wrapper(self, self.method)
    def __call__(self, *args, **kwds):
        if self.method is None:
            self.method = args[0]
            self.check_method()
            return self
        args, kwds = self.map_args_kwds(args, kwds)
        res = self.method(*args, **kwds)
        if self.res_map is not None:
            res = self.res_map(res)
        return res

