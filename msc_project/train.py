from typing import Tuple
import torch
from torch.optim import adagrad, adamax
from torch.nn.modules.module import Module

T_destination = Module.T_destination

class Trainer:
    """Used for training neural networks with a given dataset, criterion and
    optimizer
    """

    def __init__(self, train_loader, criterion, optimizer,
                 scheduler=None, device=None):
        self.train_loader = train_loader
        self.criterion = criterion
        self.optimizer = optimizer
        self.scheduler = scheduler
        self.device = device

    def __call__(self, *args, **kwargs): self.run_epochs(*args, **kwargs)

    def to(self, device): self.device = device

    def criterion_loss(self, model, data, target):
        output = model(data)
        loss = self.criterion(output, target)
        return loss

    def get_loss(self, model, data, target, kd):
        "Use distilled knowledge if available or just return criterion loss"
        if kd is None:
            return self.criterion_loss(model, data, target)
        else:
            return kd.loss(model, data, target, self.criterion)

    def run_epoch(self, model, epoch_idx, kd=None):
        "Run a single epoch"
        model.train()
        for batch_idx, (data, target) in enumerate(self.train_loader):
            data, target = data.to(self.device), target.to(self.device)
            self.optimizer.zero_grad()
            loss = self.get_loss(model, data, target, kd)
            loss.backward()
            self.optimizer.step()
            if batch_idx % 10 == 0:
                print(f'** {batch_idx} / {len(self.train_loader)} of epoch {epoch_idx}\tLoss: {loss.item()}')

    def best_metric_state(self, model, best_metric, best_state, metric_func, minimize) -> Tuple[float, T_destination]:
        """Compute metric on current model and then compare it to provided best metric,
        returning best metric state which is either the provided one, or
        current model state

        :param model: Model that is being trained
        :param best_metric: Best metric value thus far
        :param best_state: State dict associated with best metric thus far
        :param metric_func: Function to compute metric
        :param minimize: If true, then aim to minimise metric value
        :returns: (best metric, best state)

        """
        metric = metric_func(model)
        is_better = minimize and metric < best_metric
        is_better = is_better or not(minimize) and metric > best_metric
        print(f"Old = {best_metric}, new = {metric}")
        print(f"Better = {is_better}")
        if is_better:
            return metric, model.state_dict()
        else:
            return best_metric, best_state

    def run_epochs(self, model, epochs=1,
                   metric_func=None, minimize_metric=True, kd=None):
        "Run number of epochs specified by `epochs`"
        self.ensure_device()

        if metric_func is not None:
            best_metric = metric_func(model)
            best_state = model.state_dict()

        for epoch_idx in range(epochs):
            print(f'* Epoch {epoch_idx}')
            self.run_epoch(model, epoch_idx, kd)
            if self.scheduler is not None:
                self.scheduler.step()
            if metric_func is not None:
                best_metric, best_state = self.best_metric_state(
                    model,
                    best_metric,
                    best_state,
                    metric_func,
                    minimize_metric
                )

        if metric_func is not None:
            model.load_state_dict(best_state)
            return best_metric
        else:
            return None

    def ensure_device(self):
        if self.device is None:
            self.device = torch.device("cuda" if torch.cuda.is_available()
                                       else "cpu")
import msc_project.keywords as kw
import torch.optim as optim

optimizers = {
    kw.optimizers.sgd: optim.SGD,
    kw.optimizers.adadelta: optim.Adadelta,
    kw.optimizers.adagrad: optim.Adagrad,
    kw.optimizers.adam: optim.Adam,
    kw.optimizers.adamax: optim.Adamax
}

def get_optimizer(name, params, *args, **kwargs):
    return optimizers[name](params, *args, **kwargs)

schedulers = {
    kw.schedulers.multi: optim.lr_scheduler.MultiStepLR,
    kw.schedulers.step: optim.lr_scheduler.StepLR,
    kw.schedulers.exp: optim.lr_scheduler.ExponentialLR,
    kw.schedulers.cos: optim.lr_scheduler.CosineAnnealingLR,
    kw.schedulers.cyclic: optim.lr_scheduler.CyclicLR,
    kw.schedulers.one_cycle: optim.lr_scheduler.OneCycleLR,
    kw.schedulers.constant: None

}
