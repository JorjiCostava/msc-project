from typing import Tuple
import torch
from torch.functional import Tensor
from torch.utils.data import DataLoader
from torch.nn.modules.loss import _Loss
from torchvision import datasets, transforms
from torchvision.transforms.transforms import RandomHorizontalFlip

def get_kwds():
    "Get typical keyword arguments for DataLoader class"
    return {'num_workers': 1, 'pin_memory': True} if torch.cuda.is_available() else {}

def get_imagenet(data_dir, train_batch_size, test_batch_size) -> Tuple[DataLoader, DataLoader, _Loss]:
    """Get imagenet training set, test set and criterion

    :param data_dir: Directory to download imagenet to
    :param train_batch_size: Training batch size
    :param test_batch_size: Testing batch size
    :returns: (train loader, test loader, criterion)
    """
    kwds = get_kwds()

    normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                         std=[0.229, 0.224, 0.225])
    transform = transforms.Compose([
            transforms.Resize(256), transforms.CenterCrop(224),
            transforms.ToTensor(),
            normalize
    ])
    train_loader = DataLoader(
            datasets.ImageNet(data_dir, train=True, download=True,transform=transform
    ), batch_size=train_batch_size, shuffle=True, **kwds)
    val_loader = DataLoader(datasets.ImageNet(
            data_dir, train=False, download=True,
            transform=transform,
    ), batch_size=test_batch_size, shuffle=True, **kwds)
    criterion = torch.nn.CrossEntropyLoss()

    return train_loader, val_loader, criterion

def get_cifar(data_dir, batch_size, test_batch_size) -> Tuple[DataLoader, DataLoader, _Loss]:
    """Get cifar10 training set, test set and criterion

    :param data_dir: Directory to download cifar10 to
    :param train_batch_size: Training batch size
    :param test_batch_size: Testing batch size
    :returns: (train loader, test loader, criterion)
    """
    kwds = get_kwds()

    normalize = transforms.Normalize(mean=[0.4914, 0.4822, 0.4465],
                                     std=[0.2023, 0.1994, 0.2010])
    train_transform = transforms.Compose([
        transforms.RandomHorizontalFlip(),
        transforms.RandomCrop(32, 4),
        transforms.ToTensor(),
        normalize
    ])
    val_transform = transforms.Compose([
        transforms.ToTensor(),
        normalize
    ])
    train_loader = DataLoader(datasets.CIFAR10(
        data_dir, train=True, download=True,
        transform=train_transform,
    ), batch_size=batch_size, shuffle=True, **kwds)
    val_loader = DataLoader(datasets.CIFAR10(
        data_dir, train=False, download=True,
        transform=val_transform
    ), batch_size=test_batch_size, shuffle=False, **kwds)
    criterion = torch.nn.CrossEntropyLoss()
    return train_loader, val_loader, criterion

from . import keywords as kw


def get_dataset(name: str, data_dir: str, train_batch_size: int, test_batch_size: int) -> Tuple[DataLoader, DataLoader, _Loss]:
    """Get dataset by name

    :param name: str identifier for dataset
    :param data_dir: Download directory
    :param train_batch_size: Training batch size
    :param test_batch_size: Test batch size
    :returns: (train loader, test loader, criterion)
    """
    if name == kw.datasets.imagenet:
        return get_imagenet(data_dir, train_batch_size, test_batch_size)
    elif name == kw.datasets.cifar:
        return get_cifar(data_dir, train_batch_size, test_batch_size)
    else:
        raise ValueError(f"dataset {name} not recognised")

def dummy_input(name: str, test_batch_size: int, device: torch.device) -> Tensor:
    """Get dummy input

    :param name: str identifier for dataset
    :param test_batch_size: Test batch size
    :param device: Torch device
    :returns: Input tensor for dataset specified by `name`

    """
    if name == kw.datasets.cifar:
        return torch.randn([test_batch_size, 3, 32, 32]).to(device)
    if name == kw.datasets.imagenet:
        return torch.randn([test_batch_size, 3, 256, 256]).to(device)
    else:
        raise ValueError(f"dataset {name} not recognised")

