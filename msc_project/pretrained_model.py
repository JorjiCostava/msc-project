from torchvision import models
from torch.optim.lr_scheduler import StepLR, MultiStepLR
import torch
from . import keywords as kw

imagenet_zoo_func = {
    kw.models.resnet: models.resnet18,
    kw.models.alexnet: models.alexnet,
    kw.models.squeezenet: models.squeezenet1_1,
    kw.models.vgg: models.vgg16,
    kw.models.densenet: models.densenet161,
    kw.models.inception: models.inception_v3,
    kw.models.googlenet: models.googlenet,
    kw.models.shufflenet: models.shufflenet_v2_x2_0,
    kw.models.mobilenet: models.mobilenet.mobilenet_v3_small,
    kw.models.resnext: models.resnext50_32x4d,
    kw.models.wide_resnet: models.wide_resnet50_2,
    kw.models.mnasnet: models.mnasnet1_0
}

models.resnet18().state_dict

import torch

GITHUB_URL = "chenyaofo/pytorch-cifar-models"

def str2model(cache_dir, dataset, model_str, device):
    '''
    Instantiates model from its name
    '''
    print('Getting model', model_str)
    if dataset == kw.datasets.cifar:
        torch.hub.set_dir(cache_dir)
        return torch.hub.load(GITHUB_URL, 'cifar10_' + model_str, pretrained=True).to(device)
    if dataset == kw.datasets.imagenet:
        return imagenet_zoo_func[model_str](pretrained=True).to(device)
    raise ValueError('Model not supported')
