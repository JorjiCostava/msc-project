# from .bind_self import BindSelf
from msc_project.bind_self import BindSelf
from typing import Any, Dict, Optional, Union
from .map_method import MappedFunc
import copy
import json
from .path_parser import PathParser, PathType

def is_search_param(v):
    """Returns whether v is an nni search parameter

    :param v: Any
    :returns: True is v is a search parameter

    """
    return isinstance(v, dict) and '_type' in v and '_value' in v

def is_nested_search_param(p, c):
    """Returns whether parameter dict p and choice c are a nested search parameter

    :param p: The choice search parameter
    :param c: A nested search parameter of p
    :returns: Whether c can be a nested search parameter of p

    """
    return isinstance(c, dict) and '_name' in c and is_search_param(p)

class SearchSpace:
    """Encapsulates a search space that can be used in an nni experiment, or the
    parameters returned by such a search space
    """

    root_val: Union[dict, list] # Object stored at root path
    parser: PathParser
    path_map: Dict[PathType, Any] # Maps to any element nested under `root_val` by path
    aliases: Dict[PathType, PathType] # Used to map shortcuts to the canonical path under `root_val`

    def __init__(self, value: Optional[Union[dict, list]]=None,
                 parser: PathParser=PathParser('.')):
        if value is None:
            value = {}
        self.parser = parser
        if not isinstance(value, (dict, list)):
            raise TypeError('Only dict or list search spaces supported')
        self.root_val = value
        self.path_map = {}
        self.aliases = {}
        self.generate_map()

    __iter__ = BindSelf().root_val.__iter__()

    def is_nested_search_param(self, path: PathType):
        """True if `path` maps to a nested search parameter

        :param path: Path of nested choice
        :returns: True if location of a nested search parameter
        """
        if self.parser.depth(path) < 2:
            return False
        param = self._get(self.parser.subpath(path, 0, -2))
        value = self._get(self.parser.subpath(path, 0, -1))
        choice = self._get(path)
        if not is_search_param(param):
            return False
        if not is_nested_search_param(param, choice):
            return False
        if not isinstance(value, list):
            return False
        return True

    def expand_aliases(self, path):
        """Convert aliases to their canonical paths

        E.g. Convert pruner._value[0] to pruner.fpgm

        :param path: Alias or canonical path
        :returns: Canonical path

        """
        for i in range(1, len(path)+1):
            subpath = path[:i]
            if subpath in self.aliases:
                path = self.parser.join(self.aliases[subpath], path[i:])
        return path

    def add_aliases(self, path):
        """Infers aliases and returns a human readable path

        E.g. pruner.fpgm rather than pruner._value[0]

        :param path: Path to add aliases to
        :returns: Path with aliases

        """
        ret = self.parser.ROOT_PATH
        i = 0
        for j in range(2, len(path)+1):
            if not self.is_nested_search_param(path[0:j]):
                continue
            subpath = path[i:j]
            choice = self._get(path[0:j])
            ret = self.parser.join(ret, self.parser.subpath(subpath, i, j-2), choice['_name'])
            i = j
        ret = self.parser.join(ret, path[i:])
        return ret

    def parse_path(self, path):
        """Parse delimited path str to path representation

        :param path: Path to parse
        :returns: Parsed path

        """
        parsed = self.parser.parse(path)
        return self.expand_aliases(parsed)

    def _get(self, path):
        """Get value from `path_map` or `value` if is root path

        :param path: Path to get
        :returns: Value

        """
        return self.root_val if path == self.parser.ROOT_PATH else self.path_map[path]

    @MappedFunc(arg=parse_path)
    def __getitem__(self, path: Union[str, PathType]):
        """Get value associated with path

        :param path: Path
        :returns: Value

        """
        return self._get(path)

    @MappedFunc(arg=parse_path)
    def __setitem__(self, path: Union[str, PathType], value):
        """Set value associated with `path` to `value`

        To allow lists to always have enough capacity, setting a list index
        actually invokes the list.insert method

        Example::

        >>> ss = SearchSpace([1,2])
        >>> ss[5] = 16
        >>> ss[1] = 4
        >>> ss
        [1, 4, 16]


        :param path: Path of location to set
        :param value: Value to store

        """
        self.set_key_value(path, value)

    @MappedFunc(arg=parse_path)
    def __contains__(self, path: Union[str, PathType]):
        """Check if path is associated with a value in search space
        """
        return path in self.path_map or path == self.parser.ROOT_PATH

    @staticmethod
    def move_branch(src_space, src_path: Union[str, PathType],
                    dst_space, dst_path: Union[str, PathType]):
        """Move branch at a location in one search space to a location in another
        search space

        :param src_space: Search space to move from
        :param src_path: Path in search space to move from
        :param dst_space: Search space to move to
        :param dst_path: Path in search space to move to
        """
        src_path = src_space.parser.parse(src_path)
        dst_path = src_space.parser.parse(dst_path)
        removed = src_space.pop(src_path)
        if isinstance(removed, (dict, list)):
            removed = SearchSpace(removed, parser=dst_space.parser)
        dst_space[dst_path] = removed

    def __pop_from_value(self, path: PathType):

        root_path, key = self.parser.part_leaf(path)

        self._get(root_path).pop(key)

    def __pop_from_pathmap(self, path: PathType):
        if path in self.path_map:
            remove_keys = {k for k in self.path_map.keys()
                           if self.parser.descendent(path, k)}
            for k in remove_keys:
                self.path_map.pop(k)
            remove_aliases = [
                k for k, v in self.aliases.items() if v in remove_keys
            ]
            for k in remove_aliases:
                self.aliases.pop(k)
            self.path_map.pop(path)

    @MappedFunc(arg=parse_path)
    def pop(self, path: Union[str, PathType]):
        """Remove `path` and all its descendents

        :param path: Path on top of all values to remove
        :returns: The value that was removed

        """
        if path == self.parser.ROOT_PATH:
            tmp = self.root_val
            self.root_val = {}
            self.generate_map()
            return tmp

        removed = self.path_map[path]
        self.__pop_from_value(path)
        self.__pop_from_pathmap(path)
        return removed

    @MappedFunc(arg=parse_path)
    def create_path(self, path: Union[str, PathType], val = None):
        """Ensure that path can be set to a value without KeyError, by inferring values
        to store in each key excluding the leaf.

        If `val` is set then the leaf key will be set to that value

        :param path: Path to ensure can be set
        :param val: Value to store at leaf

        Example::

        >>> ss = SearchSpace(parser=PathParser('/'))
        >>> ss.create_path('rectangle/vertices[0]/x')
        >>> ss
        {
            "rectangle": {
                "vertices": [
                    {}
                ]
            }
        }
        >>> ss.create_path('rectangle/vertices[1]/x', 1)
        >>> ss
        {
            "rectangle": {
                "vertices": [
                    {},
                    {
                        "x": 1
                    }
                ]
            }
        }
        """
        if path in self.path_map or path == self.parser.ROOT_PATH:
            return
        ptr = self.root_val
        for i, k in enumerate(path[:-1]):
            root_path = path[:i+1]
            if root_path not in self.path_map:
                new_dict = self.parser.infer_type(path, i)
                self.path_map[root_path] = new_dict
                if isinstance(k, int):
                    ptr.insert(k, new_dict)
                else:
                    ptr[k] = new_dict
            ptr = self.path_map[root_path]
        if val is not None and len(path) != 0 and path not in self.path_map:
            k = self.parser.key(path, -1)
            if isinstance(ptr, list):
                ptr.insert(k, val)
            else:
                ptr[k] = val
            self.path_map[path] = val

    @MappedFunc(arg=parse_path)
    def set_key_value(self, path: Union[str, PathType],  value):
        """Set value associated with `path` to `value`

        To allow lists to always have enough capacity, setting a list index
        actually invokes the list.insert method

        Example::

        >>> ss = SearchSpace([1,2])
        >>> ss.set_key_value(5, 16)
        >>> ss.set_key_value(1, 4)
        >>> ss
        [1, 4, 16]


        :param path: Path of location to set
        :param value: Value to store
        """
        if isinstance(value, SearchSpace):
            self.set_search_space(path, value)
        elif isinstance(value, (dict, list)):
            self.set_search_space(path, SearchSpace(value, parser=self.parser))
        else:
            if path == self.parser.ROOT_PATH:
                raise TypeError("Root can only be set to SearchSpace or dict/list type")
            if path in self:
                self.pop(path)
            root_path, key = self.parser.part_leaf(path)
            self.create_path(path)
            root_val = self._get(root_path)
            if isinstance(root_val, list):
                root_val.insert(key, value)
            else:
                root_val[key] = value
            self.path_map[path] = value

    @MappedFunc(arg=parse_path)
    def update_key_value(self, path, value):
        """Update value associated with `path` to contain elements of `value`

        If `value` is list then elements will be extended

        If `value` is dict then keys will be updated

        Example::

        >>> ss = SearchSpace([1,4])
        >>> ss.update_key_value('', 16)
        >>> ss
        [1, 4, 16]


        :param path: Path of location to set
        :param value: Value to store
        """
        if isinstance(value, SearchSpace):
            self.update_search_space(path, value)
        elif isinstance(value, (dict, list)):
            self.update_search_space(path, SearchSpace(value, parser=self.parser))
        else:
            raise TypeError(f"Update method intended for structured value not {type(value)}")

    @MappedFunc(arg=parse_path)
    def update_search_space(self, path, search_space):
        """Update path using the path_map, aliases and value of a search_space,
        but keep old values unless there are matching dict keys

        Aliases are qualified by prefixing them by `path`

        :param path: Path to update
        :param search_space: Search space whose values are being updated
        """
        if  isinstance(search_space.root_val, list):
            val = []
        else:
            val = {}
        self.create_path(path, val)
        value = self._get(path)
        if isinstance(value, dict) and isinstance(search_space.root_val, dict):
            value.update(search_space.root_val)
        elif isinstance(value, list) and isinstance(search_space.root_val, list):
            value.extend(search_space.root_val)
        else:
            raise TypeError(f"Can't update value {type(value)} {type(search_space.root_val)}")
        self.path_map.update({self.parser.join(path, key): value
                              for key, value in search_space.path_map.items()
                              })
        self.aliases.update({self.parser.join(path, alias): self.parser.join(path, aliased)
                             for alias, aliased in search_space.aliases.items()
        })

    @MappedFunc(arg=parse_path)
    def set_search_space(self, path, search_space):
        """Set path using the path_map, aliases and value of a search_space,
        deleting path and all its descendents first

        Aliases are qualified by prefixing them by `path`

        :param path: Path to set
        :param search_space: Search space whose values are being set to
        """
        if path in self: self.pop(path)
        self.update_search_space(path, search_space)

    @MappedFunc(arg=parse_path)
    def set_sample(self, path, search_type, *values):
        """Create a search of the types listed at
        https://nni.readthedocs.io/en/latest/Tutorial/SearchSpaceSpec.html#types

        So for instance {"_type": "randint", "_value": [lower, upper]} becomes
        SearchSpace().set_search_param('', 'randint', lower, upper)

        Example::

        >>> ss = SearchSpace()
        >>> ss.set_search_param('', 'uniform', 0, 1)
        >>> ss
        {
            "_type": "uniform",
            "_value": [0, 1]
        }
        """
        param_dict = {'_type': search_type, '_value': list(values)}
        self.set_key_value(path, param_dict)

    @MappedFunc(arg=parse_path)
    def set_subsearch_sample(self, path, *names):
        """Create a nested subsearch of choice type


        :param path: Path to insert nested subsearch space

        https://github.com/microsoft/nni/blob/5f0a7c9ad983d051be4fc8776be04f986213af85/examples/trials/mnist-nested-search-space/search_space.json
        Code to create layer0::
        >>> conv, max_pool, avg_pool = (SearchSpace() for _ in range(3))
        >>> conv.set_sample('kernel_size', 'choice', 1, 2, 3, 5)
        >>> max_pool.set_sample('pooling_size', 'choice', 2, 3, 5)
        >>> avg_pool.set_sample('pooling_size', 'choice', 2, 3, 5)
        >>> ss = SearchSpace()
        >>> ss.set_subsearch_sample('', 'Empty', 'Conv', 'Max_pool', 'Avg_pool')
        >>> for k, v in zip(('Conv', 'Max_pool', 'Avg_pool'), (conv, max_pool, avg_pool)):
        ...   ss.update_key_value(ss.parser[k], v)
        >>> ss
        {
             "_type": "choice",
             "_value": [
                  {"_name": "Empty"},
                  {
                       "_name": "Conv",
                        "kernel_size": {
                            "_type": "choice",
                            "_value": [1, 2, 3, 5]
                        }
                   },
                   {
                       "_name": "Max_pool",
                       "pooling_size": {
                            "_type": "choice",
                            "_value": [2, 3, 5]
                       }
                   },
                   {
                       "_name": "Avg_pool",
                       "pooling_size": {
                          "_type": "choice",
                          "_value": [2, 3, 5]
                       }
                    }
             ]
        }
        """
        names_list = []
        for name in names:
            name_dict = {'_name': name}
            names_list.append(name_dict)
        param_dict = {'_type': 'choice',
                      '_value': names_list}
        self.set_key_value(path, param_dict)

        # Add later as set_key_value will invoke pop(path)
        for i, name_dict, name in zip(range(len(names_list)), names_list, names):
            # self.path_map[self.parser.join(path, name)] = name_dict
            # self.path_map[self.parser.join(path, name, '_name')] = name
            full_path = self.parser.join(path, '_value', i)
            alias = self.parser.join(path, name)
            self.aliases[alias] = full_path

    @MappedFunc(arg=(None, parse_path))
    def save(self, fn, src_path=None):
        """Save json serialisation of value at path `src_path` to file `fn`

        :param fn: Filename to save to
        :param src_path: Path of branch to serialise
        """
        if src_path is None:
            src_path = self.parser.ROOT_PATH
        with open(fn, 'w') as fp:
            json.dump(self._get(src_path), fp, indent=4, sort_keys=True)

    @MappedFunc(arg=(None, parse_path, parse_path))
    def load(self, fn, src_path=None, dst_path=None):
        """Load file and move path `src_path` in json file to
        the path `dst_path` in `self`

        :param fn: Filename to load from
        :param src_path: Path of branch to moved from loaded space
        :param dst_path: Path of branch to replace in this space

        """
        if src_path is None:
            src_path = self.parser.ROOT_PATH
        if dst_path is None:
            dst_path = self.parser.ROOT_PATH
        with open(fn, 'r') as fp:
            lspace = SearchSpace(json.load(fp), parser=self.parser)
        SearchSpace.move_branch(lspace, src_path, self, dst_path)

    def generate_map(self, path: PathType=None, value=None):
        """Walk value and generate `path_map` mappings from its references

        :param path: Path constructed thus far
        :param value: Value of most recent node
        """
        if path is None:
            path = self.parser.ROOT_PATH
            self.path_map.clear()
            value = self.root_val
        else:
            self.path_map[path] = value
        if isinstance(value, dict):
            for k, v in value.items():
                self.generate_map(self.parser.join(path, k), v)
            if is_search_param(value):
                values = value['_value']
                for i, v in enumerate(values):
                    if is_nested_search_param(value, v):
                        alias = self.parser.join(path, v['_name'])
                        full_path = self.parser.join(path, '_value', i)
                        self.aliases[alias] = full_path
        if isinstance(value, list):
            for i, v in enumerate(value):
                self.generate_map(self.parser.join(path, i), v)
        if self.is_nested_search_param(path):
            alias = self.parser.join(self.parser.subpath(path, 0, -2), self._get(self.parser.join(path, '_name')))
            self.aliases[alias] = path

    def __copy__(self):
        ret = SearchSpace(parser=copy.copy(self.parser))
        ret.root_val = copy.copy(self.root_val)
        ret.path_map = copy.copy(self.path_map)
        return ret

    def get_sorted_paths(self, descending=True):
        """Sort paths by distance to the root

        :param descending: If True then furthest paths first
        :returns: List of paths

        """
        sort_func = lambda entry: self.parser.depth(entry[0])
        return sorted(self.path_map.items(), key=sort_func, reverse=descending)

    def __deepcopy__(self, memo=None):
        ret = SearchSpace(parser=copy.deepcopy(self.parser))
        ret.aliases = copy.copy(self.aliases)
        ret.path_map = copy.copy(self.path_map)
        ret.root_val = copy.copy(self.root_val)
        for path, val in self.path_map.items():
            ret.path_map[path] = copy.copy(val)
        for path, val in ret.get_sorted_paths():
            if self.parser.depth(path) == 1:
                 ret.root_val[path[0]] = copy.copy(val)
                 continue
            root, key = self.parser.part_leaf(path)
            ret.path_map[root][key] = ret.path_map[path]
        return ret

    def tree_paths(self, prefix):
        """Generator of paths as they should be displayed in human-readable tree view

        :param prefix: Path to place at root of tree
        :returns:
        """
        for path in sorted(self.path_map.keys()):
            if not self.parser.descendent(prefix, path):
                continue
            key = self.parser.key(path, -1)
            if key in ('_name', '_type'):
                continue
            val = self._get(self.parser.join(prefix, path))
            path = self.add_aliases(path)
            new_path = []
            for k in path:
                if isinstance(k, int):
                    k = self.parser.stringify(self.parser[k])
                new_path.append(k)
            path = tuple(new_path)
            if '_value' in path:
                continue
            yield path[len(prefix):], val

    def line_len(self, path: PathType, vert_len, dash_len):
        """Length of line to display leaf of `path` given length of vertical lines from
        parent nodes and dashed lines to leaf

        :param path: Path to measure tree output line
        :param vert_len: Length of a single vertical line and subsequent space characters
        :param dash_len: Length of a sequence of dashed line and subsequence space character
        :returns: Number of characters to display line containing leaf of path

        E.g.

        ├── layer0                 |        layer0 <- CHOICE(Empty, Conv, Max_pool, Avg_pool)
        │   ├── Empty              |

        vert_len is length of vertical bar passing from parent of layer0 and
        subsequent spaces i.e. len('│ ')

        dash_len is length of dashed and vbar characters connecting parent node
        to leaf i.e. len('├──')

        """
        ret = vert_len * (self.parser.depth(path) - 1)
        ret += dash_len
        _, key = self.parser.part_leaf(path)
        ret += len(str(key))
        return ret

    def tree(self, path='', vert='|', dash='-', vdash='|', llcorner='|'):
        """Display search space in human-readable tree representation

        :param path: Path to place at root of tree (not displayed)
        :param vert: str to use for displaying vertical lines
        :param dash: str to use for displaying horizontal lines
        :param vdash: str to use for displaying vertical line connecting to a
        dash on the right
        :param llcorner: str to use for a lower left corner
        :returns:

        """
        path = self.parse_path(path)
        branch_width = 2
        vert_part = ' ' * (branch_width + 1)
        vert_part_len = len(vert_part) + len(vert)
        dash_part = dash * branch_width + ' '
        llc_dash_part_len = len(dash_part) + len(llcorner)
        vd_dash_part_len = len(dash_part) + len(vdash)

        res = []

        sorted_paths = tuple(self.tree_paths(path))

        last = {}
        max_len = 0
        max_key_len = 0

        dash_part_len = max(llc_dash_part_len, vd_dash_part_len)
        for i, (k,v) in enumerate(sorted_paths):
            root, key  = self.parser.part_leaf(k)
            last[root] = k
            max_len = max(max_len, self.line_len(k, vert_part_len, dash_part_len))
            max_key_len = max(max_key_len, len(str(key)))

        for i, (k,v) in enumerate(sorted_paths):
            root, key = self.parser.part_leaf(k)
            for i in range(len(root)):
                isub = root[:i]
                jsub = root[:i+1]
                try:
                    if last[isub] == jsub:
                        sym = ' '
                    else:
                        sym = vert
                except KeyError as e:
                    raise Exception(f'{e}\nPATH={k}\nKEYS = {last}')
                res.append(sym + vert_part)
            if last[root] == k:
                sym = llcorner
                dash_part_len = llc_dash_part_len
            else:
                sym = vdash
                dash_part_len = vd_dash_part_len
            res.append(sym + dash_part)
            res.append(str(key))
            res.append(' ' * (max_len - self.line_len(k, vert_part_len, dash_part_len)) + ' ')
            res.append('  |  ')
            key_value_part = str(key).rjust(max_key_len) + ' <- '
            if is_search_param(v):
                res.append(key_value_part)
                type = v['_type'].upper()
                values = v['_value']
                if isinstance(values[0], dict):
                    values = (x['_name'] for x in values)
                values = ', '.join(map(str,values))
                res.append(f'{type}({values})')
            elif not isinstance(v, dict) and not isinstance(v, list):
                res.append(key_value_part)
                res.append(f'{str(v)}')
            res.append('\n')
        return ''.join(res)

    def __str__(self):
        return json.dumps(self.root_val, indent=4, sort_keys=True)
    def __repr__(self):
        return str(self)
    def __eq__(self, o):
        if not isinstance(o, SearchSpace):
            return False
        return self.root_val == o.root_val

def load_search_space(fn, *args, **kwargs):
    """Instantiate search space from json file

    :param fn: Filename of json file
    :returns: Search space

    """
    ss = SearchSpace(*args, **kwargs)
    ss.load(fn)
    return ss

import os

def loaded_space_choice(subspace: SearchSpace, parent_dir=None, fns=None):
    """Create a choice of nested subspaces with each subspace loaded from a file in
    `fns`

    :param subspace: Subspace to load the files to
    :param parent_dir: Parent directory to find all the filenames
    :param fns: Filenames in the directory
    :returns:

    """
    if parent_dir is None and fns is None:
        return
    if parent_dir is None:
        parent_dir = os.curdir
    if fns is None:
        fns = os.listdir(parent_dir)
    print(f"Loading files: {', '.join(fns)}")
    subspace.set_subsearch_sample('', *fns)
    for fn in fns:
        loaded = load_search_space(os.path.join(parent_dir, fn), parser=PathParser('/'))
        subspace.update_key_value(fn, loaded)
    print("Resulting search space")
    display_search_space(subspace)

def displaystr_search_space(ss, path=''):
    "Convenience method for displaying search space tree with unicode box chars"
    return ss.tree(path=path, vdash='├', dash='─',vert='│',llcorner='└')

def display_search_space(ss, path=''):
    "Convenience method for printing search space tree with unicode box chars"
    print(displaystr_search_space(ss, path), end='')
