import importlib
def dynamic_func(path, table=locals()):
    """Load a function dynamically from a path written like a typical import
    statement

    :param path: A module path string ending in the function that needs to be loaded
    :param table: Namespace to look for function if function isn't qualified by
    a module. Defaults to builtin functions.

    :returns: Function or class
    """
    if '.' in path:
        mod_path, name = path.rsplit('.', 1)
        mod = importlib.import_module(mod_path)
        return getattr(mod, name)
    else:
        return table[path]

import re

from functools import wraps
non_escaped_pattern = re.compile(r'(?<!\\)\s')

def escaped_split(s):
    "Split on whitespace excluding any escaped spaces"
    if s == '':
        return []
    return [sub.replace('\\ ', ' ') for sub in non_escaped_pattern.split(s)]

def get_argument_index(text, line, begidx, endidx):
    "Position of argument in the sequence of arguments"
    toks = escaped_split(line)
    start = line.find(toks[0])
    end = start + len(toks[0])
    for arg_idx, tok in enumerate(toks[1:]):
        if tok == '':
            return arg_idx
        start = line.find(tok, end)
        end = start + len(tok)
        if begidx >= start and begidx <= end:
            return arg_idx
    return -1

def complete_args_switch(func):
    """Wrapper for cmd.Cmd.complete_ methods that instead requires them to return a
    tuple of methods, which will be invoked to handle the completion depending
    on the position of the argument to autocomplete in the sequence of arguments
    """
    @wraps(func)
    def wrapper(self, text, line, begidx, endidx):
        funcs = func(self, text, line, begidx, endidx)
        if not isinstance(funcs, tuple):
            funcs = (funcs,)
        arg_idx = get_argument_index(text, line, begidx, endidx)
        if arg_idx >= 0 and arg_idx < len(funcs):
            return funcs[arg_idx](text, line, begidx, endidx)
        else:
            return []
    return wrapper
