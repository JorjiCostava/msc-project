from copy import deepcopy
from msc_project.bind_self import BindSelf
from msc_project.map_method import MappedFunc
from .map_dict import MapDict
from .search_space import PathParser, SearchSpace
import operator

class SearchView(SearchSpace):
    """Create a view of descendents of a given path in a search space with all
    paths relative that path"""
    def __init__(self, search_space=None, path_prefix='', *args, **kwds):
        if search_space is None:
            search_space = SearchSpace(*args, **kwds)
        path_prefix = search_space.parse_path(path_prefix)
        search_space.create_path(path_prefix, {})
        self.search_space = search_space
        self.path_prefix = path_prefix
        self._path_map = MapDict(search_space.path_map, self.add_prefix, self.rem_prefix)
        self.aliases = MapDict(search_space.aliases,
                               self.add_prefix, self.rem_prefix,
                               self.add_prefix, self.rem_prefix)
        self.parser = search_space.parser

    def set_root_val(self, value):
        self.search_space[self.path_prefix] = value

    def get_root_val(self):
        return self.search_space[self.path_prefix]

    root_val = property().getter(get_root_val).setter(set_root_val)

    def get_path_map(self):
        return self._path_map

    def set_path_map(self, value):
        self._path_map.clear()
        self._path_map.update(value)

    path_map = property().getter(get_path_map).setter(set_path_map)

    parse_path = BindSelf().parser.parse()

    @MappedFunc(arg=parse_path)
    def add_prefix(self, path):
        return self.parser.join(self.path_prefix, path)
    @MappedFunc(arg=parse_path)
    def rem_prefix(self, path):
        if self.parser.descendent(self.path_prefix, path):
            return path[len(self.path_prefix):]
        return None
    def __deepcopy__(self, memo=None):
        search_space = deepcopy(self.search_space)
        return SearchView(search_space, self.path_prefix)
    @MappedFunc(arg=parse_path)
    def __getitem__(self, path):
        """Changes getitem to instead return a search view of the given path, unless it
        is a stop value that can't be indexed

        :param path: 
        :returns:

        """
        self.create_path(path, {})
        v = super(SearchView, self).__getitem__(path)
        if isinstance(v, dict):
            return SearchView(self.search_space, self.add_prefix(path))
        else:
            return v
    def __eq__(self, o):
        if isinstance(o, SearchSpace):
            return super().__eq__(o)
        else:
            return self.root_val == o

