from .bind_self import BindSelf
from .map_method import MappedFunc

def call_if_not_none(f, x):
    return f(x) if f is not None else x

from functools import wraps
class MapDict:
    """Internally modifies a contained dictionary through an interface to a subset
    of that dictionary with different keys and values (provided by reversable
    mapping functions). Only the internal dictionary is stored in memory and can
    continue to be modified directly.

    E.g.
    >>> compress = lambda s: zlib.compress(s.encode())
    >>> decompress = lambda s: zlib.decompress(s).decode() if isinstance(s, bytes) else None
    >>> d = MapDict(map_k=compress, unmap_k=decompress, map_v=compress, unmap_v=decompress)
    >>> d['hello world'] = 'fizzbuzz'
    >>> d
    {'hello world': 'fizzbuzz'}
    >>> d._dict
    {b'x\x9c\xcbH\xcd\xc9\xc9W(\xcf/\xcaI\x01\x00\x1a\x0b\x04]': b'x\x9cK\xcb\xac\xaaJ*\xad\xaa\x02\x00\x0f\xaa\x03\x8f'}

    """
    def __init__(self, inner_dict=None, map_k = None, unmap_k = None, map_v = None, unmap_v = None):
        if inner_dict is None:
            inner_dict = {}
        if not isinstance(inner_dict, dict):
            raise TypeError("Inner dict must be dict")
        self._dict = inner_dict
        self._map_k = map_k
        self._unmap_k = unmap_k
        self._map_v = map_v
        self._unmap_v = unmap_v
    def map_key(self, k):
        """Applies mapping to key f: K -> K'

        :param k: dict key
        :returns: key after mapping
        """
        return call_if_not_none(self._map_k, k)
    def checked_map_key(self, k):
        mk = self.map_key(k)
        if mk is None and k is not None:
            raise KeyError(f"Received signal that key {k} can't be mapped")
        return mk
    def unmap_key(self, k):
        """Reverses key mapping f^-1: K' -> K

        :param k: dict key
        :returns: key before mapping
        """
        return call_if_not_none(self._unmap_k, k)
    def checked_unmap_val(self, v):
        mv = self.unmap_val(v)
        if mv is None and v is not None:
            raise ValueError(f"Received signal that value {v} can't be unmapped")
        return mv
    def map_val(self, v):
        return call_if_not_none(self._map_v, v)
    def checked_map_val(self, v):
        mv = self.map_val(v)
        if mv is None and v is not None:
            raise ValueError(f"Received signal that value {v} can't be mapped")
        return mv
    def unmap_val(self, v):
        return call_if_not_none(self._unmap_v, v)
    def unmap_item(self, item):
        """Applies key unmapping to (key, val) pair. A None return value signals
        the key is excluded from the dictionary (hence mapped dict is a subset)

        :param item: dict (K',V')
        :returns: dict (K,V)

        """
        k, v = self.unmap_key(item[0]), self.unmap_val(item[1])
        if (k is None and item[0] is not None) or\
           (v is None and item[1] is not None):
            return None
        return k, v

    BINDED_DICT = BindSelf()._dict

    __getitem__ = MappedFunc(BINDED_DICT.__getitem__()).arg(checked_map_key).result(checked_unmap_val)
    get = MappedFunc(BINDED_DICT.get()).arg(checked_map_key).result(checked_unmap_val)
    pop = MappedFunc(BINDED_DICT.pop()).arg(checked_map_key).result(checked_unmap_val)
    __setitem__ = MappedFunc(BINDED_DICT.__setitem__()).arg(checked_map_key, checked_map_val)
    setdefault = MappedFunc(BINDED_DICT.setdefault()).arg(checked_map_key, checked_map_val).result(unmap_val)
    __delitem__ = MappedFunc(BINDED_DICT.__delitem__()).arg(checked_map_key)

    def popitem(self):
        """Removes the last non-nil mapping in _dict.keys()

        :returns: Item (K,V)

        """
        for k, v in reversed(self._dict.items()):
            unmapped_item = self.unmap_item((k, v))
            if unmapped_item is not None:
                del self._dict[k]
                return unmapped_item
        raise KeyError('popitem(): dictionary is empty')

    def keys(self):
        """Returns a list of keys with the mapping reversed with nil excluded

        :returns: filter object

        """
        return (i[0] for i in self.items())

    def clear(self):
        for key in self.keys():
            self.pop(key)

    __iter__ = keys

    def items(self):
        """Returns a list of items with the mapping reversed with nil excluded

        :returns: filter object

        """
        # return x for x in  map(self.unmap_item, self._dict.items())
        for x in self._dict.items():
            if x is None:
                yield None
            mx = self.unmap_item(x)
            if mx is not None:
                yield mx

    def values(self):
        return (i[1] for i in self.items())

    def update(self, mapping, **kwargs):
        if hasattr(mapping, 'keys') and callable(mapping.keys):
            for k in mapping:
                self[k] = mapping[k]
        elif mapping is not None:
            for k, v in mapping:
                self[k] = v
        for k, v in kwargs.items():
            self[k] = v

    @classmethod
    def fromkeys(cls, iterable, value=None, map_k = None, unmap_k = None, map_v = None, unmap_v = None):
        return cls(dict.fromkeys(iterable, value), map_k, unmap_k, map_v, unmap_v)

    def __str__(self):
        return repr(self)
    def __repr__(self):
        return '{' + ', '.join(f'{repr(k)}: {repr(v)}' for k, v in self.items()) + '}'
