import torch
import msc_project.keywords as kw

class Evaluator:
    """Used for evaluating neural networks with a given test set, criterion and
    a list of metric identifiers used to configure which metrics are computed and returned
    """
    def __init__(self, test_loader, device,
                 criterion=None, metrics=kw.metrics.values()):

        self.test_loader = test_loader
        self.criterion = criterion
        self.device = device
        self.metrics = metrics
        if self.metric_enabled(kw.metrics.loss) and self.criterion is None:
            raise AttributeError("instantiate with criterion to evaluate loss")

    def metric_enabled(self, metric):
        return metric in self.metrics

    def __call__(self, model, *args, **kwargs):
        return self.evaluate(model, *args, **kwargs)

    def evaluate(self, model):
        "Return dictionary of metrics specified by `self.metrics`"
        model.eval()

        loss = 0
        correct = 0

        eval_loss = self.metric_enabled(kw.metrics.loss)
        eval_accuracy = self.metric_enabled(kw.metrics.accuracy)

        print(f'* Evaluate loss = {eval_loss}, acc = {eval_accuracy}')

        with torch.no_grad():
            i = 0
            for data, target in self.test_loader:
                data, target = data.to(self.device), target.to(self.device)
                output = model(data)

                if eval_loss:
                    loss += self.criterion(output, target).item()
                if eval_accuracy:
                    pred = output.argmax(dim=1, keepdim=True)
                    correct += pred.eq(target.view_as(pred)).sum().item()
                print(f"** Evaluation {100*i/len(self.test_loader)}% finished")
                i += 1

        result = {}

        if eval_loss:
            loss /= len(self.test_loader.dataset)
            result[kw.metrics.loss] = loss

        if eval_accuracy:
            acc = 100 * correct / len(self.test_loader.dataset)
            result[kw.metrics.accuracy] = acc

        print(f"Evaluation result = {result}")

        return result

def accuracy_evaluator(test_loader, device):
    "Convenience method for an accuracy evaluator"
    tester = Evaluator(test_loader, device=device, eval_accuracy=True)
    return tester
