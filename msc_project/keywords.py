class dotdict(dict):
    __getattr__ = dict.get
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__

models = dotdict({
    'resnet': 'resnet20',
    'alexnet': 'alexnet',
    'squeezenet': 'squeezenet1_1',
    'vgg': 'vgg16',
    'densenet': 'densenet_161',
    'inception': 'inception_v3',
    'googlenet': 'googlenet',
    'shufflenet': 'shufflenet_v2',
    'mobilenet': 'mobilenet_v3',
    'resnext': 'resnext50',
    'wide_resnet': 'wide_resnet50',
    'mnasnet': 'mnasnet1_0',
})

datasets = dotdict({
    'cifar': 'cifar10',
    'imagenet': 'imagenet'
})

metrics = dotdict({
    'accuracy': 'top_1accuracy',
    'loss': 'loss',
    'flops': 'flops',
    'params': 'parameters'
})

targets = dotdict({
    'flops': 'flops',
    'params': 'parameters',
    'latency': 'latency'
})

# These can be dynamically loaded via loaded_space_choice but kept for convenience

pruners = dotdict({
    'l1': 'l1filter',
    'l2': 'l2filter',
    'fpgm': 'fpgm',
    'mean_rank': 'activation_mean_rank',
    'apoz_rank': 'activation_apoz_rank',
    'taylor': 'taylor_fo_weight',
})

optimizers = dotdict({
    'adadelta': 'adadelta',
    'sgd': 'sgd',
    'adam': 'adam',
    'adagrad': 'adagrad',
    'adamax': 'adamax',
})

schedulers = dotdict({
    'multi': 'multi_step',
    'step': 'step',
    'exp': 'exponential',
    'cos': 'cosine_annealing',
    'cyclic': 'cyclic',
    'one_cycle': 'one_cycle',
    'cos_warm': 'cosine_annealing_warm_restarts',
})
