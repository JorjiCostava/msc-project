import cmd
from msc_project.util import escaped_split
import re
escaped_pattern = re.compile(r'(?P<sub>(\S+\\ )+)$')

from functools import wraps


def allow_escaped_whitespace(func):
    """Wrapper for cmd.Cmd.complete_ methods that expand begidx and endidx to
    include escaped spaces
    """
    @wraps(func)
    def wrapper(self, text, line, begidx, endidx, *args, **kwargs):
        before = line[:begidx]
        m = escaped_pattern.search(before)
        if m:
            sub = m['sub']
            text = sub.replace('\\ ', ' ') + text
            begidx, endidx = m.span()
            endidx -= sub.count('\\ ') * len('\\ ')
        completions = func(self, text, line, begidx, endidx, *args, **kwargs)
        return [completion.replace(' ', '\\ ') for completion in completions]
    return wrapper


class SearchSpaceCmd(cmd.Cmd):
    "Cmd program with useful methods for manipulating search spaces"
    def __init__(self, search_space):
        import readline
        delims = readline.get_completer_delims()
        delims = ''.join(c for c in delims if c not in '/[]')
        readline.set_completer_delims(delims)
        super(SearchSpaceCmd, self).__init__()
        self.search_space = search_space

    @allow_escaped_whitespace
    def path_complete(self, text, line, begidx, endidx):
        "Return list of possible paths in search space"
        paths = self.search_space.path_map.keys()
        paths = set(map(self.search_space.parser.stringify, map(self.search_space.add_aliases, paths)))
        sep = self.search_space.parser.sep
        text = escaped_split(line[:endidx])[-1]
        # print()
        # print(list(paths))
        strip_prefix = self.search_space.parser.sep
        if text:
            ret = [
                path for path in paths
                if path.startswith(text) and path.find(sep, len(text)) == -1
            ]
            return ret
        else:
            return [path for path in paths if path.find(sep) == -1]

    @allow_escaped_whitespace
    def file_complete(self, text, line, begidx, endidx, root_dir=None):
        "Return list of file path completions from direct children in file tree"
        import os
        full_text = escaped_split(line[:endidx])[-1]
        if root_dir is not None:
            full_text = os.path.join(root_dir, full_text)
        text_dir = os.path.dirname(full_text)
        text_dir_abs = os.path.abspath(text_dir)
        text_abs = os.path.abspath(full_text)
        fns = []
        for root, dirs, files in os.walk(text_dir_abs):
            if root == text_dir_abs:
                fns.extend(os.path.join(text_dir, fn) for fn in files if os.path.join(root, fn).startswith(text_abs))
                fns.extend(os.path.join(text_dir, directory,'') for directory in dirs if os.path.join(root, directory).startswith(text_abs))
        return fns
