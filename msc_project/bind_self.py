class BindSelf:
    """
    Delegate a class-level function to an attribute of the instance

    E.g.

    >>> class Multiline:
    ...   def __init__(self):
    ...     self.__list = []
    ...   line = BindSelf().__list.append()
    ...   def __str__(self):
    ...     return '\n'.join(self.__list)
    ...
    >>> m = Multiline()
    >>> m.line('hello')
    >>> m.line('world')
    >>> print(m)
    hello
    world
    """
    def __init__(self, attrs=None):
        if attrs is None:
            attrs = []
        self.attrs = attrs
    def __getattr__(self, name):
        return BindSelf(self.attrs + [name])
    def __call__(self):
        attrs = self.attrs
        def wrapper(self, *args, **kwds):
            f = self
            for a in attrs:
                f = getattr(f, a)
            return f(*args, **kwds)
        return wrapper
