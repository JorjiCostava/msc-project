import json
from msc_project.path_parser import PathParser
from msc_project.search_space import display_search_space
from msc_project.search_view import SearchView
from msc_project import search_values
from msc_project.util import escaped_split, complete_args_switch
from msc_project.search_space_cmd import SearchSpaceCmd

def get_parser():
    parser = ArgumentParser("Tool to create an nni search space")
    parser.add_argument('--load', type=str, default=None,
                        help='Loads a JSON file to continue modification')
    parser.add_argument('--silence-cmd', action='store_true', help='Stops cmd output')
    return parser

from msc_project.search_space import SearchSpace, loaded_space_choice
from argparse import ArgumentParser

from functools import partial

class CreateSearchSpace(SearchSpaceCmd):
    """Cmd for loading, saving and editing search space files via commands that
    link to SearchSpace API
    """
    intro = 'Welcome to search space editor. Type help or ? to list commands\n'
    prompt = '(editor) '

    def __init__(self, search_space):
        super(CreateSearchSpace, self).__init__(search_space)

    def silence(self):
        CreateSearchSpace.intro = None
        CreateSearchSpace.prompt = ''

    def do_empty(self, line):
        'Empty search space: Empty'
        self.search_space = SearchSpace(parser=PathParser('/'))

    def do_exit(self, line):
        'Exit: EXIT'
        return True

    def do_tree(self, line):
        'Display in tree structure: TREE'
        args = escaped_split(line)
        if len(args) == 0:
            path = input('Enter path> ')
        else:
            path = args[0]
        display_search_space(self.search_space, path)

    @complete_args_switch
    def complete_tree(self, text, line, begidx, endidx):
        return self.path_complete

    def do_print(self, line):
        'Print a branch of the tree: PRINT square.vertices'
        args = escaped_split(line)
        if len(args) == 0:
            path = input('Enter path> ')
        else:
            path = args[0]
        print(self.search_space[path])

    @complete_args_switch
    def complete_print(self, text, line, begidx, endidx):
        return self.path_complete

    def do_sep(self, line):
        'Set seperator to use for specifying paths '
        args = escaped_split(line)
        if len(args) == 0:
            sep = input('Enter path seperator> ')
        else:
            sep = args[0]
        self.search_space.parser.sep = sep

    def do_fsubsearch(self, line):
        'Set a path to hold a parameter choice between loaded search spaces'
        args = escaped_split(line)
        if len(args) == 0:
            root_dir = input('Enter search spaces dir> ')
        else:
            root_dir = args[0]
        if len(args) < 2:
            fns = escaped_split(input('Enter filenames'))
        else:
            fns = args[1:]
            if len(fns) == 0:
                fns = None
        loaded_space_choice(SearchView(self.search_space), root_dir, fns)

    @complete_args_switch
    def complete_fsubsearch(self, text, line, begidx, endidx):
        args = escaped_split(line)[1:]
        subfile_complete = partial(self.file_complete, root_dir = args[0])
        ret = (self.file_complete, subfile_complete, subfile_complete, subfile_complete, subfile_complete, subfile_complete, subfile_complete)
        return ret

    def do_set(self, line):
        'Set a path to a value: SET square.vertices.lr [5,7]'
        args = escaped_split(line)
        if len(args) == 0:
            path = input('Enter path> ')
        else:
            path = args[0]
        if len(args) < 2:
            val = json.loads(input('Enter value in json format> '))
        else:
            val = json.loads(' '.join(args[1:]))
        self.search_space.set_key_value(path, val)

    @complete_args_switch
    def complete_set(self, text, line, begidx, endidx):
        return self.path_complete

    def do_update(self, line):
        'Set a path to update with elements of value: UPDATE square.vertices [{"x":1,"y":2},{"x":0,"y":0}]'
        args = escaped_split(line)
        if len(args) == 0:
            path = input('Enter path> ')
        else:
            path = args[0]
        if len(args) < 2:
            val = json.loads(input('Enter value in json format> '))
        else:
            val = json.loads(' '.join(args[1:]))
        self.search_space.set_key_value(path, val)

    @complete_args_switch
    def complete_update(self, text, line, begidx, endidx):
        return self.path_complete

    def do_alias(self, line):
        'Set a path to be accessible via an alias: ALIAS square.vertices[0] sequare.vertices.lr'
        args = escaped_split(line)
        if len(args) == 0:
            path = input('Enter path> ')
        else:
            path = args[0]
        if len(args) < 2:
            alias = input('Enter alias> ')
        else:
            alias = args[1]
        path, alias = map(self.search_space.parse_path, (path, alias))
        self.search_space.aliases[alias] = path

    @complete_args_switch
    def complete_alias(self, text, line, begidx, endidx):
        return self.path_complete, self.path_complete

    @complete_args_switch
    def complete_update(self, text, line, begidx, endidx):
        return self.path_complete

    def do_remove(self, line):
        'Remove a path: REMOVE square.vertices.lr'
        args = escaped_split(line)
        if len(args) == 0:
            path = input('Enter path> ')
        else:
            path = args[0]
        self.search_space.pop(path)

    @complete_args_switch
    def complete_remove(self, text, line, begidx, endidx):
        return self.path_complete

    def do_load(self, line):
        'Load a json file to a specific path: LOAD fgpm.json pruners.fpgm'
        args = escaped_split(line)
        if len(args) == 0:
            fn = input('Enter filename> ')
        else:
            fn = args[0]
        if len(args) < 2:
            src_path = input('Enter source branch> ')
        else:
            src_path = args[1]
        if len(args) < 3:
            dst_path = input('Enter destination branch> ')
        else:
            dst_path = args[2]
        self.search_space.load(fn, src_path, dst_path)

    @complete_args_switch
    def complete_load(self, text, line, begidx, endidx):
        return self.file_complete, self.path_complete, self.path_complete

    def do_save(self, line):
        'Save to a json file: SAVE search_space.json'
        args = escaped_split(line)
        if len(args) == 0:
            fn = input('Enter filename> ')
        else:
            fn = args[0]
        self.search_space.save(fn)

    @complete_args_switch
    def complete_save(self, text, line, begidx, endidx):
        return self.file_complete

    def do_move(self, line):
        'Move a branch to a specific point: MOVE pruners.fpgm pruners.l1filter'
        args = escaped_split(line)
        if len(args) == 0:
            src_path = input('Enter source path> ')
        else:
            src_path = args[0]
        if len(args) < 2:
            dst_path = input('Enter destination path> ')
        else:
            dst_path = args[1]
        SearchSpace.move_branch(self.search_space, src_path,
                                self.search_space, dst_path)

    @complete_args_switch
    def complete_move(self, text, line, begidx, endidx):
        return self.path_complete, self.path_complete

    def json_option(self, args):
        if len(args) == 0:
            values = escaped_split(input('Enter values in choice> '))
        else:
            values = args
        return map(json.loads,values)

    values_funcs = {
        'choice': search_values.choice,
        'subsearch': search_values.subsearch,
        'randint': search_values.randint,
        'uniform': search_values.lower_upper,
        'quniform': search_values.lower_upper_q,
        'qloguniform': search_values.lower_upper_q,
        'loguniform': search_values.lower_upper,
        'normal': search_values.lower_upper,
        'qnormal': search_values.lower_upper_q,
        'qlognormal': search_values.lower_upper_q,
        'lognormal': search_values.lower_upper,
    }

    def do_search(self, line):
        'Store a search parameter: SEARCH fruit choice "apple" "orange" "pair"'
        args = escaped_split(line)
        if len(args) == 0:
            path = input('Enter path> ')
        else:
            path = args[0]
        if len(args) < 2:
            search_type = input('Enter search type> ')
        else:
            search_type = args[1]
        if search_type not in self.values_funcs:
            self.stdout.write('*** Unknown search type: %s\n'%search_type)
        values = self.values_funcs[search_type](args[2:])
        if search_type == 'subsearch':
            self.search_space.set_subsearch_sample(path, *values)
        else:
            self.search_space.set_sample(path, search_type, *values)

    def search_complete(self, text, line, begidx, endidx):
        return [choice for choice in self.values_funcs.keys() if choice.startswith(text)]

    @complete_args_switch
    def complete_search(self, text, line, begidx, endidx):
        return self.path_complete, self.search_complete

    def __call__(self): self.cmdloop()

def main(args):
    search_space = SearchView(parser=PathParser('/'))
    if args.load is not None:
        search_space.load(args.load)
    console = CreateSearchSpace(search_space)
    if args.silence_cmd:
        console.silence()
    console()

if __name__=='__main__':
    parser = get_parser()
    args = parser.parse_args()
    main(args)
