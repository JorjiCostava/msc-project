from msc_project.path_parser import PathParser
from msc_project.search_view import SearchView
from msc_project.finetune_kd import KnowledgeDistiller
from msc_project.evaluate import Evaluator
from msc_project.train import Trainer, get_optimizer
from msc_project.data import dummy_input, get_dataset
import msc_project.keywords as kw
import logging
from msc_project.search_space import display_search_space

_logger = logging.getLogger('experiment')

import os
import torch

import nni
from nni.compression.pytorch import ModelSpeedup
from nni.compression.pytorch.utils.counter import count_flops_params

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
print('Using device:', device)

from msc_project.pretrained_model import str2model

def substitute_pruner_args(args, model, criterion, evaluator, train_loader, dummy_input_tensor, optimizer):
    "Substitute pruner arguments that must be instantiated in code"
    if 'optimizer' in args:
        args['optimizer'] = optimizer
    if 'dummy_input' in args:
        args['dummy_input'] = dummy_input_tensor
    if 'trainer' in args:
        def train_func(model, optimizer, criterion, epoch):
            print(f'Invoked train_func({type(model)},{optimizer},{criterion},{epoch})')
            try:
                trainer = Trainer(train_loader, criterion, optimizer)
                trainer.run_epoch(model, epoch)
            except Exception as e:
                print(e)
            print('Finished train_func')
        args['trainer'] = train_func
    if 'criterion' in args:
        args['criterion'] = criterion
    if 'model' in args:
        args['model'] = model
    if 'evaluator' in args:
        args['evaluator'] = evaluator

def get_adjustment(params):
    """Get adjustment multiplier that needs to be applied to all sparsities in
    order to get the desired total sparsity when factoring in number of
    parameters in each layer

    """
    if not params['search_layers']:
        return 1
    # Current number of params pruned
    config_list = (params['config_list', i, 'sparsity'] for i in params['config_list'])
    config_list_params = (params['config_list_params', i] for i in params['config_list_params'])
    current_pruned = sum(cfg * cfg_params
                         for cfg, cfg_params
                         in zip(config_list, config_list_params))
    print(f'current {current_pruned}')
    goal_pruned = params['config_list_total_params'] * 0.25
    print(f'Aim to prune {goal_pruned} of {params["config_list_total_params"]} prunable parameters')
    adjustment = goal_pruned / current_pruned
    print(f'Depressing all sparsities by a factor of {adjustment}')
    return adjustment

def get_pruner(params, model, criterion, evaluator, train_loader, dummy_input_tensor, optimizer):
    "Load pruner and substitute config list and args from search space"
    pruner_func = dynamic_func(params['pruner/func'])
    for i in range(len(params['config_list'].root_val)):
        params.update_key_value(f'config_list/{i}', params['pruner', 'config_list'])
    adjustment = get_adjustment(params)
    config_list = []
    for i in params['config_list']:
        group = params['config_list', i]
        sparsity = min(group['sparsity'] * adjustment, 0.9)
        print(sparsity)
        group["sparsity"] = sparsity
        if params['search_layers']:
            group_params = params['config_list_params', i]
            print(f'group sparsity * params = {sparsity} * {group_params} = {sparsity * group_params}'.ljust(80) + str(group['op_names']))
        config_list.append(group.root_val)
    pruner_args = params['pruner/args'].root_val
    substitute_pruner_args(pruner_args, model, criterion, evaluator, train_loader, dummy_input_tensor, optimizer)
    assert None not in pruner_args.values()
    print(pruner_args['trainer'])
    print(f'pruner cfg = {config_list}')
    return pruner_func(model, config_list, **pruner_args)

def substitute_optimizer_args(args, model):
    "Substitute optimizer arguments that must be instantiated in code"
    if 'params' in args:
        args['params'] = model.parameters()

def get_optimizer(params, model):
    "Load pruner and substitute args from search space"
    optimizer_func = dynamic_func(params['optimizer/func'])
    optimizer_args = params['optimizer/args'].root_val
    substitute_optimizer_args(optimizer_args, model)
    print(f'optimizer args = {optimizer_args}')
    assert None not in optimizer_args.values()
    return optimizer_func(**optimizer_args)

def substitute_scheduler_args(args, optimizer, params, train_loader):
    "Substitute scheduler arguments that must be instantiated in code"
    if 'epochs' in args:
        args['epochs'] = params['epochs']
    if 'steps_per_epoch' in args:
        args['steps_per_epoch'] = len(train_loader)
    if 'optimizer' in args:
        args['optimizer'] = optimizer
    if 'base_lr' in args:
        args['base_lr'] = (1 - args['max_lr'])*params['scheduler/cycle_amp_scale']
    if 'milestones' in args:
        number_of_milestones = int(params['scheduler/milestones/_name'][:-1])
        milestones = []
        for i in range(number_of_milestones):
            milestones.append(params['scheduler', 'milestones', str(i)])
        milestones.sort()
        args['milestones'] = milestones
    if 'cycle_momentum' in args:
        args['cycle_momentum'] = 'momentum' in optimizer.defaults
    args['verbose'] = True

def get_scheduler(params, optimizer, train_loader):
    "Load scheduler and substitute args from search space"
    scheduler_func = dynamic_func(params['scheduler/func'])
    scheduler_args = params['scheduler/args'].root_val
    substitute_scheduler_args(scheduler_args, optimizer, params, train_loader)
    print(f'scheduler args = {scheduler_args}')
    assert None not in scheduler_args.values()
    return scheduler_func(**scheduler_args)

def fine_tune(model, trainer, evaluator, epochs, kd):
    "Fine tune compressed model"
    def evaluate_accuracy(model):
        best_acc = evaluator(model)[kw.metrics.accuracy]
        print(f"Reporting epoch accuracy {best_acc}")
        nni.report_intermediate_result(best_acc)
        return best_acc
    return trainer.run_epochs(model, epochs, evaluate_accuracy, False, kd)

def target_missed(params, results, metric, small_is_good):
    "Returns whether target is missed"
    target = params[f'target.{metric}']

    if target is None:
        return False

    if small_is_good:
        is_missed = results[metric] > target
    else:
        is_missed = results[metric] < target
    if is_missed:
        for k in results:
            results[k] = 0
        nni.report_final_result(results)
        print(f"Missed {metric} target {target}")
        print(results)

def main(params):
    """Run an experiment for search space

    :param params: Search space parameters
    """
    display_search_space(params)

    train_loader, value_loader, criterion = get_dataset(
        params['dataset'],
        params['cache_dir'],
        params['train_batch_size'],
        params['test_batch_size']
    )

    dummy_input_tensor = dummy_input(params['dataset'], params['test_batch_size'], device)

    model = str2model(params['cache_dir'], params['dataset'], params['model'], device)
    print(model)
    print('Loaded model')
    optimizer = get_optimizer(params, model)
    print(optimizer)
    print('Loaded optimiser')
    scheduler = get_scheduler(params, optimizer, train_loader)
    print(scheduler)
    print('Loaded scheduler')

    import copy
    if params['kd']:
        kd = KnowledgeDistiller(copy.deepcopy(model), 4)
    else:
        kd = None

    trainer = Trainer(train_loader, criterion, optimizer, scheduler, device)
    evaluator = Evaluator(value_loader, device, criterion, metrics=params['metrics'])

    pruner = get_pruner(params, model, criterion, evaluator, train_loader, dummy_input_tensor, optimizer)
    print(pruner)
    print('Loaded pruner')

    model = pruner.compress()


    if kw.metrics.flops in params['metrics'] or kw.metrics.params in params['metrics']:
        flops_before, params_before, _ = count_flops_params(model, dummy_input_tensor)
        print(f'Before pruned: flops={flops_before}, params={params_before}')
        print(f'In json={params["params"]}')

    pruner.get_pruned_weights()

    experiment_id = nni.get_experiment_id()
    trial_id = nni.get_trial_id()
    uid = f'{experiment_id}_{trial_id}'

    model_path = os.path.join(params['experiment_dir'], f'pruned_{uid}')
    mask_path = os.path.join(params['experiment_dir'], f'mask_{uid}')
    pruner.export_model(model_path=model_path, mask_path=mask_path)

    pruner._unwrap_model()
    m_speedup = ModelSpeedup(model, dummy_input_tensor, mask_path, device)
    m_speedup.speedup_model()

    results = {}

    # print(params['target'].__contains__('latency'))
    # if kw.metrics.latency in params['target']:
    #     results[kw.metrics.latency] = NotImplemented

    #     if target_missed(params, results, kw.metrics.latency, True):
    #         return

    if kw.metrics.flops in params['target'] or kw.metrics.params in params['metrics']:
        flops, total_params, _ = count_flops_params(model, dummy_input_tensor)
        perc_reduction = lambda before, after: 100 * (before - after) / before
        flops_reduction = perc_reduction(flops_before, flops)
        params_reduction = perc_reduction(params_before, total_params)
        results[kw.metrics.flops] = flops_reduction
        results[kw.metrics.params] = params_reduction
        print(f"Reduced flops by {flops_reduction}% and params by {params_reduction}%")

        # if target_missed(params, results, kw.metrics.flops, True):
        #     return
        # if target_missed(params, results, kw.metrics.params, True):
        #     return

    results[kw.metrics.accuracy] = fine_tune(model, trainer, evaluator, params['epochs'], kd)

    # if kw.metrics.accuracy in params['target']:
    #     if target_missed(params, results, kw.metrics.accuracy, False):
    #         return

    print("Final result")
    print(results)
    nni.report_final_result(results[kw.metrics.accuracy])

from msc_project.search_space import SearchSpace
from msc_project.util import dynamic_func

from argparse import ArgumentParser

def get_parser():
    parser = ArgumentParser("Runs an nni experiment")
    parser.add_argument('--load-dir', type=str, default=None)
    parser.add_argument('--path', type=str, default=None)
    return parser

if __name__ == '__main__':
    args = get_parser().parse_args()
    params = SearchView(SearchSpace(nni.get_next_parameter(), parser=PathParser('/')))
    if args.load_dir is not None:
        params.load(args.load_dir)
    if args.path is not None:
        params = params[args.path]
    main(params)

# def test():
#     params = SearchSpace()
#     params.load("search_space.json")
# e   train_loader, value_loader, criterion = get_dataset(
#         params['dataset'],
#         params['cache_dir'],
#         params['train_batch_size'],
#         256
#     )
#     evaluator = Evaluator(value_loader, device, criterion, metrics=params['metrics'])
#     model = str2model(params['cache_dir'], params['dataset'], params['model'], device)
#     return evaluator(model)

# def test2():
#     params = SearchSpace()
#     params.load("search_space.json")
#     model = str2model(params['cache_dir'], params['dataset'], params['model'], device)
#     dummy_input_tensor = dummy_input(params['dataset'], params['test_batch_size'], device)
#     flops, total_params, _ = count_flops_params(model, dummy_input_tensor)
#     for name, layer in (x for x in model.named_modules() if isinstance(x[1], torch.nn.Conv2d)):
#         if name == '':
#             continue
#         num = sum(p.numel() for p in model.get_submodule(name).parameters())
#         print(f'{name}: {num}')
#     return flops, total_params

